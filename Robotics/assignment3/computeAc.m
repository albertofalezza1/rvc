function [A, c] = computeAc(ts, qn, dqi, dqf)
    A = zeros(length(qn)-2, length(qn));
    c = [];
    for k = 1 : length(qn)-2
        tk = ts(k);
        tk1 = ts(k + 1);
        tk2 = ts(k + 2);
        qk = qn(k);
        qk1 = qn(k+1);
        qk2 = qn(k+2);
        Tk = tk1 - tk;
        Tk1 = tk2 - tk1;
        ck = (3*(Tk1/Tk)*(qk1 - qk)) + (3*(Tk/Tk1)*(qk2-qk1));
        a = [Tk1 2*(Tk + Tk1) Tk];
        A(k, k:k+length(a)-1) = a;
        c = [c; ck];
    end
    % first column multiplied by initial velocity
    ai = A(:, 1)*dqi;
    % last column multiplied by final velocity
    af = A(:, length(qn))*dqf;
    % remove the columns we don't need anymore to make it quadratic
    A(:, length(qn)) = [];
    A(:, 1) = [];
    % remove from c, ai and af
    c = c - ai - af;
end

