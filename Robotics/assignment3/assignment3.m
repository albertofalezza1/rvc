close all;
clc;

tic

ts = [0 2 4 8 10];
qn = [10 20 0 30 40];
dqi = 0; dqf = 0; % initial & final velocities
ar_t=[]; r_q=[]; r_v=[]; r_a=[];
thomas = false;

phase1 = toc;
%% FORCED VELOCITY IN PATH POINTS
% Interpolating polynomials with computed velocities at path points 
% and imposed velocity at initial | final points
tic

vel = [dqi];

for k = 2:length(qn)-1
    dqk = velocitiesK(qn(k-1), qn(k), qn(k+1),ts(k-1), ts(k), ts(k+1));
    vel = [vel dqk];
end

vel = [vel dqf];

phase2 = toc;

%% FORCED ACCELERATIONS IN PATH POINTS
% Interpolating polynomials with continuous accelerations at path points 
% and imposed velocity at initial | final points

tic

vel = [dqi];

[A, c] = computeAc(ts, qn, dqi, dqf);
if ~thomas
    vel = [vel (A\c).']; % (A\c).' = (inv(A)*c).'
else
    vel = [vel thomasAlgorithm(A, c)];
end

vel = [vel dqf];

phase2 = toc;

%% PLOT MULTIPOINT TRAJECTORY PLANNING
tic

for k = 1:length(qn)-1
    [q,v,a,r_t] = multiTrajPlan(qn(k),qn(k+1),ts(k),ts(k+1),vel(k),vel(k+1));
    
    r_q = [r_q q];
    r_v = [r_v v];
    r_a = [r_a a];
    ar_t = [ar_t r_t];
end

position = subplot(3,1,1);
plot(ar_t, r_q)
title('position');
grid on
velocity = subplot(3,1,2);
plot(ar_t, r_v)
title('velocity');
grid on
acceleration = subplot(3,1,3);
plot(ar_t, r_a)
title('acceleration');
grid on

timeElapsed = toc+phase1+phase2