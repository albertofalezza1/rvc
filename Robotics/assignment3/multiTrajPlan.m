function [q,v,a,r_t] = multiTrajPlan(qki, qkf, r_tki, tkf, dqki, dqkf)
%FORCEVELMULTI 
    syms qk(t) ak3 ak2 ak1 ak0 tki
    
    Tk = tkf-r_tki;
    
    r_ak0 = qki;
    r_ak1 = dqki;
    r_ak2 = (1/Tk)*(((3*(qkf - qki))/Tk) - 2*dqki - dqkf);
    r_ak3 = (1/(Tk.^2))*((2*(qki - qkf))/Tk + dqki + dqkf);
    
    r_t = linspace(r_tki, tkf, 1000);
    
    qk(t) = ak3*(t - tki).^3 + ak2 * (t - tki).^2 + ak1 * (t - tki) + ak0;
    dqk(t) = diff(qk);
    ddqk(t) = diff(dqk);
    
    s_qk(t) = subs(qk(t),[ak3, ak2, ak1, ak0, tki],[r_ak3, r_ak2, r_ak1, r_ak0, r_tki]);
    s_dqk(t) = subs(dqk(t),[ak3, ak2, ak1, ak0, tki],[r_ak3, r_ak2, r_ak1, r_ak0, r_tki]);
    s_ddqk(t) = subs(ddqk(t),[ak3, ak2, ak1, ak0, tki],[r_ak3, r_ak2, r_ak1, r_ak0, r_tki]);
    
    q = s_qk(r_t);
    v = s_dqk(r_t);
    a = s_ddqk(r_t);
end

