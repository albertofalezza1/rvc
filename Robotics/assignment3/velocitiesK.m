function [dqk] = velocitiesK(q1,q2,q3,t1,t2,t3)
%VELOCITIESK

    v1 = (q2 - q1)/(t2 - t1);
    v2 = (q3 - q2)/(t3 - t2);
        
    if sign(v1) ~= sign(v2)
        dqk = 0;
    else
        dqk = (v1 + v2)/2;
    end
end

