# RoboticsVisionControl 2

This folder contains the Unity project for the second exercise.

## Settings

To load this project you can:

    - open Unity Hub -> click on "open" button -> choose this folder and confirm

When Unity project starts you could see an interface like this:

![Unity env 2](/doc/img/ur5_2.png "Unity env")

Unity has three main parts: 
- Hierarchy: on the left side of the environment
- Browser: on the bottom of the environment
- Inspector: on the right side of the environment

Now it is necessary to have set correctly ROS2 node in your colcon workspace. If you didn't do it you can find the guide in ROS2Node folder

## Run simulator
If you click play button to run the simulation, the robot should move itself to the initial position.
If the configuration is correct, while Unity is running, you can open the command line and type this command to see the topics list:
``
ros2 topic list
``
If you see a list of topics, the environment is correctly configured and you can procede.

### Control type
Before you write your node you have to set the control type for the robot (modify the control loop with simulation stopped).
Actually, you can choose between a joint position controller or a cartesian position controller.
To enable or disable one of two controllers you have to:
- Select UR5 GameObject on the Hierarchy
- Go to Inspector side and move where all scripts are
- Between the scripts there should be a "Joint controller" and "Cartesian controller" scripts. If you enable or disable the check box on the left side of scripts name you switch the type of control.

![Unity scripts](/doc/img/Scripts.PNG "Unity scripts")

In parallel you always have a manual joints controller.
Using the slider on the left side of the scene you can modify the joints position (the manual joint control works only if there aren't any joints publisher)



### Sphere target selection
In the scene you have a yellow sphere with some small white spheres on it surface. If you click with a mouse on the white spheres they change color and become blue. Only when three sphere are clicked the respective points will be published


![Sphere](/doc/img/sphere.png "Sphere")

### Topic list

When you run the Unity simulator and on command line type:

``
ros2 topic list
``

a list of topics like in this image should appear.

![Topics](/doc/img/sphere_topic.png "Topics")

Topics:

- /sphere_\<number>/pose: this topic defines the pose of the selected sphere. The pose will be published only when all of the three spheres are selected. The message type is Pose, where the position and orientation of the sphere expressed in robot base is defined.
Unity publishes while ROS2 subscribes

-  /joint_state: actual joints configuration. Unity publish while ROS2 subscribe

- /ur5/command/joint_\<number joint>: this topics allow to move the manipulator joints. If you publish on this topic the manipulator joints will move in target position. The message type is Float32 and the position is in radians. Unity subscribes ROS2 publishes

- /wsg_50/controller/command: this topic allows to controll the gripper. The message type is Bool. If you publish true the gripper will close, while if you publish false the gripper will open.
Unity subscribes ROS2 publishes

- /ur5/IK_joints: this is an internal topic don't use it.


### ROS2 launch file

ROS2 side you have a package rvc_bringup, in the package you have two launch files in folder "launch".

- cartesian_bringup.launch.py: if in Unity you choose the cartesian controller in ROS2 you have to launch this launch file. When you run this launch file another topic will be created:
    - /ur5/ee_target/pose: this topic allows to receive target pose message for the cartesian controller. The message type is Pose and the pose has to be expressed in base frame
    - /ur5/ee_actual/pose: this topic allows to read the end-effector pose message for the joint controller. The message type is Pose and the pose has to be expressed in base frame

- joint_bringup.launch.py: if in Unity you choose the joint controller in ROS2 you have to launch this launch file. When you run this launch file another topic will be created:
    - /ur5/ee_actual/pose: this topic allows to read the end-effector pose message for the joint controller. The message type is Pose and the pose has to be expressed in base frame

## Your node

In order to move the robot correctly you have to create ROS2 node that implements the trajectory.
To do this you have to follow some steps:
- Create your ROS2 package. You can choose the language that you prefer: python or c++ 

- Define which topics you have to subscribe or publish

- Implement the main loop with the frequency that you want with the trajectory

An example in cartesian space could be:

- Run simulator and launch the correct launch file for the cartesian control
- Select the sphere where you want to move the robot
- Subscribe to the sphere topics to get the pose
- Subscribe to the actual robot end-effector pose topic
- Define a publisher on cartesian target points (/ur5/ee_target/pose)
- Implememnt a control loop with trajectory and publish a new point each iteration
- Reach the position

In joint space it is the same but you have to publish the new position for each joint.

In the reference section you can find some tutorials.

## Reference

ROS2
- [Create ROS2 package](https://docs.ros.org/en/foxy/Tutorials/Creating-Your-First-ROS2-Package.html)
- [Create publisher subscriber node python](https://docs.ros.org/en/foxy/Tutorials/Writing-A-Simple-Py-Publisher-And-Subscriber.html)
- [Create publisher subscriber node c++](https://docs.ros.org/en/foxy/Tutorials/Writing-A-Simple-Cpp-Publisher-And-Subscriber.html)
