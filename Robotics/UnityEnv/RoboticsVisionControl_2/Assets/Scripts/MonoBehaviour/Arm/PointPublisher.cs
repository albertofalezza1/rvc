using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rclcs;

public class PointPublisher : MonoBehaviourRosNode
{
    // Start is called before the first frame update
    public string NodeName = "point_node";
    protected override string nodeName { get { return NodeName; } }
    public string topicName = "point";
    private Publisher<geometry_msgs.msg.PoseStamped> pointPub;
    public GameObject obj;
    private geometry_msgs.msg.PoseStamped msg;
    public GameObject ref_frame, second_frame;

    public GameObject ref_1, ref_2;
    public bool cube_b = false;
    public bool second = false;
    Vector3 dist;
    void Start()
    {
 
    }
    protected override void StartRos(){
        pointPub = node.CreatePublisher<geometry_msgs.msg.PoseStamped>(topicName);
        init_msgs();
    }
    // Update is called once per frame
    private void init_msgs(){
        msg = new geometry_msgs.msg.PoseStamped();
        msg.Header.Frame_id = "";

    }
    void Update()
    {
        SpinSome();
        if(cube_b){
            drawCube();
        }
        dist = ref_1.transform.position - ref_2.transform.position;
        //Debug.Log("Distance " + dist.x + " " + dist.y + " " + dist.z);
        //Debug.Log("Distance " + dist);
        msg.Header.Update(clock);
        // msg.Pose.Position.X = obj.transform.position.x;
        // msg.Pose.Position.Y = obj.transform.position.y;
        // msg.Pose.Position.Z = obj.transform.position.z;
        // msg.Pose.Position.Unity2Ros(obj.transform.position);
        // msg.Pose.Orientation.Unity2Ros(obj,transform.rotation);
        msg.Pose.Unity2Ros(obj.transform, ref_frame.transform);
        if(second){
            msg.Pose.Unity2Ros(second_frame.transform, ref_frame.transform);
        }
        // msg.Pose.Orientation.X = obj.transform.rotation.x;
        // msg.Pose.Orientation.Y = obj.transform.rotation.y;
        // msg.Pose.Orientation.Z = obj.transform.rotation.z;
        // msg.Pose.Orientation.W = obj.transform.rotation.w;
        pointPub.Publish(msg);
        // Debug.Log("Pose " + msg.Pose.Position.X);
        // Debug.Log("Pose " + msg.Pose.Position.Y);
        // Debug.Log("Pose " + msg.Pose.Position.Z);
    }
    private void drawCube(){       
        var cube = GameObject.CreatePrimitive(PrimitiveType.Plane);
        cube.name = "test";
        Renderer cube_render = cube.GetComponent<Renderer>();
        cube_render.material.color = Color.blue;
        cube_render.material.shader = Shader.Find( "Transparent/Diffuse" );
        cube.transform.position = new Vector3(ref_1.transform.position.x, ref_1.transform.position.y, ref_1.transform.position.z + 0.13f);
        cube.transform.rotation = ref_1.transform.rotation;
        cube.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);  
        cube_b = false;
    }
}
