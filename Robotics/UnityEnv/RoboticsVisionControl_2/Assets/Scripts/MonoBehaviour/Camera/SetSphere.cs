using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSphere : MonoBehaviour
{

    public GameObject center_sphere, small_sphere;
    private float x, y, z, r;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float s = 0.0f;
        float t = -0.7f;
        r = 0.2f;
        z = center_sphere.transform.position.z + (r * Mathf.Cos(s) * Mathf.Sin(t));
        x = center_sphere.transform.position.x - (r * Mathf.Sin(s) * Mathf.Sin(t));
        y = center_sphere.transform.position.y + (r * Mathf.Cos(t));

        Vector3 rel_pose = center_sphere.transform.position - small_sphere.transform.position;
        //Quaternion rotation = Quaternion.LookRotation(rel_pose, Vector3.up);
        //small_sphere.transform.rotation = Quaternion.FromToRotation(Vector3.up, rel_pose);
        Debug.DrawLine(small_sphere.transform.position, center_sphere.transform.position, Color.green);
        //drawObject();
    }
    private void drawObject(){

        var cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        cube.name = "sphere";
        Renderer cube_render = cube.GetComponent<Renderer>();
        cube_render.material.color = Color.black;
        cube_render.material.shader = Shader.Find( "Transparent/Diffuse" );
        cube.transform.position = new Vector3(x, y, z);
        cube.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);

    }   
}
