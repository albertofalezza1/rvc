using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualJointController : MonoBehaviour
{
    // Start is called before the first frame update
    public ArticulationBody baseTreeArticulation;
    private ArticulationBody[] treeArticulation;
    private float[] joint_pose = {0,0,0,0,0,0};
    public float[] joint_states = {0,0,0,0,0,0};

    void Start()
    {
        treeArticulation = baseTreeArticulation.GetComponentsInChildren<ArticulationBody>();
        initJointPose(treeArticulation);
    }

    // Update is called once per frame
    void Update()
    {
        treeArticulation[0].jointPosition = new ArticulationReducedSpace(joint_pose[0], 0, 0);
        treeArticulation[1].jointPosition = new ArticulationReducedSpace(joint_pose[1], 0, 0);
        treeArticulation[2].jointPosition = new ArticulationReducedSpace(joint_pose[2], 0, 0);
        treeArticulation[3].jointPosition = new ArticulationReducedSpace(joint_pose[3], 0, 0);
        treeArticulation[4].jointPosition = new ArticulationReducedSpace(joint_pose[4], 0, 0);
        treeArticulation[5].jointPosition = new ArticulationReducedSpace(joint_pose[5], 0, 0);
        updateJointPosition();
    }


    public void updateJointPosition(){
        for(int i = 0; i< 6; i++){
            joint_states[i] = treeArticulation[i].jointPosition[0];
        }
    }

    public void initJointPose(ArticulationBody[] treeArticulation){
        //joint_pose[0] = treeArticulation[0].jointPosition[0];
        joint_pose[0] = 0.0f;
        joint_pose[1] = -1.57f;
        joint_pose[2] = 1.57f;
        joint_pose[3] = 3.14f;
        joint_pose[4] = -1.57f;
        joint_pose[5] = -1.57f;

    }


    void OnGUI() {
        int boundary = 20;

#if UNITY_EDITOR
        int labelHeight = 20;
        GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = 20;
#else
        int labelHeight = 40;
        GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = 40;
#endif
        GUI.skin.label.alignment = TextAnchor.MiddleLeft;
        for (int i = 0; i < 6; i++) {
            GUI.Label(new Rect(boundary, boundary + ( i * 2 + 1 ) * labelHeight, labelHeight * 4, labelHeight), "Joint " + i + ": ");
            joint_pose[i] = GUI.HorizontalSlider(new Rect(boundary + labelHeight * 4, boundary + (i * 2 + 1) * labelHeight + labelHeight / 4, labelHeight * 5, labelHeight), joint_pose[i], treeArticulation[i].xDrive.lowerLimit*Mathf.Deg2Rad, treeArticulation[i].xDrive.upperLimit*Mathf.Deg2Rad);
        }
    }
}
