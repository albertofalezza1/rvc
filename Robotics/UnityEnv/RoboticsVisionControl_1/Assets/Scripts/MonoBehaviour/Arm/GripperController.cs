using System.Collections;
using System.Collections.Generic;
using rclcs;
using UnityEngine;

public class GripperController : MonoBehaviourRosNode
{
    // Start is called before the first frame update

    public string NodeName = "gripper_controller_node";
    private string topicName = "/wsg_50/controller/command";
    private Subscription<std_msgs.msg.Bool> sub;
    protected override string nodeName { get { return NodeName; } }
    public ArticulationBody finger_left;
    public ArticulationBody finger_right;
    public float[] finger_pose = {0,0};
    private bool status_gripper = false;
    float t, t1;
    float duration = 5.0f;
    public bool lock_gripper = false;
    private float actual_left, actual_right;
    public GameObject cube_1, cube_2, cube_3, cube_4;
    void Start()
    {
        initGripperPose();

    }
    protected override void StartRos(){
        sub = node.CreateSubscription<std_msgs.msg.Bool>(topicName, gripper_callback);
    }

    private void initGripperPose(){
        finger_pose[0] = finger_left.xDrive.lowerLimit;
        finger_pose[1] = finger_right.xDrive.upperLimit;
        finger_left.jointPosition = new ArticulationReducedSpace(finger_pose[0], 0, 0);   
        finger_right.jointPosition = new ArticulationReducedSpace(finger_pose[1], 0, 0);      
    }

    // Update is called once per frame
    public void Update()
    {
        SpinSome();

        finger_left.jointPosition = new ArticulationReducedSpace(finger_pose[0], 0, 0);   
        finger_right.jointPosition = new ArticulationReducedSpace(finger_pose[1], 0, 0);   
        if(status_gripper){
            if(finger_left.jointPosition[0] >= -0.026f){
                finger_pose[0] = -0.026f;
                finger_pose[1] = 0.027f;
            }else{
                finger_pose[0] = Mathf.LerpAngle(finger_left.xDrive.lowerLimit, -0.026f, t);
                finger_pose[1] = Mathf.LerpAngle(finger_right.xDrive.upperLimit, 0.027f, t);
                if (t <= 1) t += Time.deltaTime/duration;
            }

            t1=0;
            cube_1.GetComponent<GraspController>().enableKinematic();
            cube_2.GetComponent<GraspController>().enableKinematic();
            cube_3.GetComponent<GraspController>().enableKinematic();
            cube_4.GetComponent<GraspController>().enableKinematic();
            
        }else{
            finger_pose[0] = Mathf.LerpAngle(-0.026f, finger_left.xDrive.lowerLimit, t1);
            finger_pose[1] = Mathf.LerpAngle(0.027f, finger_right.xDrive.upperLimit, t1);
            if (t1 <= 1) t1 += Time.deltaTime/duration;
            t = 0;
            cube_1.GetComponent<GraspController>().disableKinematic();
            cube_2.GetComponent<GraspController>().disableKinematic();
            cube_3.GetComponent<GraspController>().disableKinematic();
            cube_4.GetComponent<GraspController>().disableKinematic();
        }
    }

    private void gripper_callback(std_msgs.msg.Bool msg)
    {
        status_gripper = msg.Data;
    }
}
