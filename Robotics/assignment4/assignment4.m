close all;
clc;

ts = [0  5  7 8 10 15 18];
qn = [0 -2 -5 0 6 12 8];
dqi = 0; dqf = 0; % initial & final velocities
ar_t=[]; r_q=[]; r_v=[]; r_a=[]; w = [];
smooth = true;

[A,c] = computeAc(ts, qn, dqi, dqf, smooth);
acc = thomasAlgorithm(A,c);

%% Cubic splines with assigned initial and final velocities, computation based on the accelerations

for k = 1:length(qn)-1
    [q,v,a,r_t] = cubicSplineMulti(qn(k),qn(k+1),ts(k),ts(k+1),acc(k),acc(k+1));
    
    r_q = [r_q q];
    r_v = [r_v v];
    r_a = [r_a a];
    ar_t = [ar_t r_t];
end

%% Cubic splines with assigned initial and final velocities, computation based on the accelerations smooth
u = 0.3;
for i = 1:length(qn)
        w = [w rand()];
end
W_inv = diag(w);
[s, dds] = lossFunction(transpose(qn), W_inv, c, A, u);
    
for k = 1:length(qn)-1
    [q,v,a,r_t] = cubicSplineMulti(s(k),s(k+1),ts(k),ts(k+1),dds(k),dds(k+1));
    
    r_q = [r_q q];
    r_v = [r_v v];
    r_a = [r_a a];
    ar_t = [ar_t r_t];
end

%% PLOT 

position = subplot(3,1,1);
plot(ar_t, r_q)
title('position');
grid on
velocity = subplot(3,1,2);
plot(ar_t, r_v)
title('velocity');
grid on
acceleration = subplot(3,1,3);
plot(ar_t, r_a)
title('acceleration');
grid on