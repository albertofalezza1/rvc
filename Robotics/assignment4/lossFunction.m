function [s,dds] = lossFunction(qv, W_inv, C, A, u)
    lambda = (1 - u)/(6*u);
    dds = inv(A + lambda*C*W_inv*transpose(C))*C*qv;
    s = qv - lambda*W_inv*transpose(C)*dds;
end

