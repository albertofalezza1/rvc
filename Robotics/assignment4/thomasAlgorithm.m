function [x] = thomasAlgorithm(A, d)
%THOMASALGORITHM 

    % Forward elimination
    for k = 2 : 1 : length(A)
        m = A(k, k-1)/A(k-1, k-1);
        A(k, k) = A(k, k) - m*A(k-1, k);
        d(k) = d(k) - m*d(k-1);
    end
    % Backward Substitution
    dn = d(length(d));
    bn = A(end);
    xn = dn/bn;
    x = zeros(1, length(A));
    x(length(A)) = xn;
    for k = length(A)-1:-1:1
        xk = (d(k) - A(k, k+1)*x(k+1))/A(k, k);
        x(k) = xk;
    end
end

