function [A, c] = computeAc(ts, qn, dqi, dqf, smooth)
    A = zeros(length(qn), length(qn));
    if smooth
        c = zeros(length(qn), length(qn));
    else
        c = [];
    end
    for k = 1 : length(qn)-2
        tk = ts(k);
        tk1 = ts(k + 1);
        tk2 = ts(k + 2);
        qk = qn(k);
        qk1 = qn(k+1);
        qk2 = qn(k+2);
        Tk = tk1 - tk;
        Tk1 = tk2 - tk1;
        a = [Tk 2*(Tk + Tk1) Tk1];
        if smooth
            ck = [6/Tk -(6/Tk + 6/Tk1) 6/Tk1];
            c(k+1, k:k+length(a)-1) = ck;
        else
            ck = 6*( ((qk2 - qk1)/Tk1) - ((qk1 - qk)/Tk) );
            c = [c; ck];
        end
        A(k+1, k:k+length(a)-1) = a;
    end
    T0 = ts(2) - ts(1);
    a = [2*T0 T0];
    A(1, 1:2) = a;
    Tn = ts(length(ts)) - ts(length(ts) - 1);
    a = [Tn 2*Tn];
    A(length(qn), length(qn)-1:length(qn)) = a;
    q0 = qn(1);
    q1 = qn(2);
    if smooth
        c0 = [-6/T0 6/T0];
        cn = [6/Tn -6/Tn];
        c(1, 1:2) = c0;
        c(length(qn), length(qn)-1:length(qn)) = cn;
    else
        c0 = [6*((q1 - q0)/T0 - dqi)];
        cn = [6*(dqf - ((qn(length(qn)) - qn(length(qn)-1))/Tn))];
        c = [c0; c];
        c = [c; cn];
    end
end

