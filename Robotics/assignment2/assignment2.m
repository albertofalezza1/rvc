%Implement in Matlab the Trapezoidal trajectory taking into account the different constraints
close all;
clc;

qi = 1;       %initial position
qf = 5;       %final position
ti = 1;       %initial time
tf = 6;      %final time
tc = 2;       %acc/dec time
dqc = 2.5;    %constant velocity
ddqc = 2.5;   %acceleration/deceleration
maxAcc = 20; %max acceleration
maxVel = 15; %max velocity
dqi = 5;    %initial velocity
dqf = 2;    %final velocity


%% Trapezoidal given tc
syms q(t)

[r_q(t), r_dq(t), r_ddq(t)] = trapezioTC(ti, tf, qi, qf, tc);

%% Trapezoidal given ddqc
syms q(t)

[r_q(t), r_dq(t), r_ddq(t)] = trapezioDDQC(ti, tf, qi, qf, ddqc);

%% Trapezoidal given dqc
syms q(t)

[r_q(t), r_dq(t), r_ddq(t)] = trapezioDQC(ti, tf, qi, qf, dqc);

%% Trapezoidal given dqc & ddqc
syms q(t)

[r_q(t), r_dq(t), r_ddq(t), tf] = trapezioDQC_DDQC(ti, qi, qf, dqc, ddqc);

%% Trapezoidal given dqi & dqf, dqc & ddqc max

trapezioDQI_DQF(qi, qf, ti, tf, maxAcc, maxVel, dqi, dqf);

%% Trapeziodal multipoints
r_q = []; r_v =[]; r_a=[]; ar_t=[];
qn = [0 20 50 40 10];
r_ti = 0;
for k=1:length(qn)-1
    
    [q,v,a,r_t,r_ti] = trapezioMulti(qn(k), qn(k+1), r_ti, maxAcc, maxVel);

    r_q = [r_q q];
    r_v = [r_v v];
    r_a = [r_a a];
    ar_t = [ar_t r_t];
end

position = subplot(3,1,1);
plot(ar_t, r_q)
title('position');
grid on
velocity = subplot(3,1,2);
plot(ar_t, r_v)
title('velocity');
grid on
acceleration = subplot(3,1,3);
plot(ar_t, r_a)
title('acceleration');
grid on

%% Trapezoidal multipoints smooth

r_q = []; r_v =[]; r_a=[]; ar_t=[];
qn = [0 20 50 40 10];
r_ti = 0;

for k=2:2:length(qn)-1
    
    [q,v,a,r_t,r_ti] = trapezioMultiSmooth(qn(k-1), qn(k), qn(k+1), r_ti, maxAcc, maxVel);

    r_q = [r_q q];
    r_v = [r_v v];
    r_a = [r_a a];
    ar_t = [ar_t r_t];
end

if rem(length(qn),2) == 0 
    % It's even 
    [q,v,a,r_t,r_ti] = trapezioMulti(qn(k+1), qn(k+2), r_ti, maxAcc, maxVel); 
 
    r_q = [r_q q]; 
    r_v = [r_v v]; 
    r_a = [r_a a]; 
    ar_t = [ar_t r_t]; 
end

position = subplot(3,1,1);
plot(ar_t, r_q)
title('position');
grid on
velocity = subplot(3,1,2);
plot(ar_t, r_v)
title('velocity');
grid on
acceleration = subplot(3,1,3);
plot(ar_t, r_a)
title('acceleration');
grid on

%% Plot

t = linspace(ti,tf,1000);

position = subplot(3,1,1);
plot(t, r_q(t))
title('position');
grid on
velocity = subplot(3,1,2);
plot(t, r_dq(t))
title('velocity');
grid on
acceleration = subplot(3,1,3);
plot(t, r_ddq(t))
title('acceleration');
grid on

