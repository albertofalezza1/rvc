function [qt,dqt,ddqt] = trapezioTC(r_ti, r_tf, r_qi, r_qf, r_tc)
%TRAPEZIOTC
syms qi qf q(t) tf ti tc ddqc dqc

%given tc (ti = 0)
    if ~isnan(r_tc)
        if r_tc <= (r_tf-r_ti)/2
            r_ddqc = (r_qf-r_qi)/(r_tc*(r_tf-r_ti)-r_tc^2);
            r_dqc = r_ddqc*r_tc;
        else
            fprintf("Error: tc > (tf-ti)/2\n");
        end
    end

q(t) = piecewise((ti<=t)&(t<=(ti + tc)),       qi + (dqc/(2*tc))*(t-ti).^2, ...
                ((ti + tc)<t)&(t<=(tf - tc)), qi + dqc*(t-ti-tc/2),  ...
                ((tf - tc)<=t)&(t<=tf),       qf - (dqc/(2*tc))*(tf - t).^2);
dq(t) = diff(q);
ddq(t) = diff(dq);


qt = subs(q(t), [ti,tf,qi,qf,tc,dqc,ddqc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_ddqc]);
dqt = subs(dq(t), [ti,tf,qi,qf,tc,dqc,ddqc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_ddqc]);
ddqt = subs(ddq(t), [ti,tf,qi,qf,tc,dqc,ddqc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_ddqc]);
end

