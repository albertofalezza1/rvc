function [qt,dqt,ddqt] = trapezioDDQC(r_ti, r_tf, r_qi, r_qf, r_ddqc)
%TRAPEZIODDQC 
syms qi qf q(t) tf ti tc ddqc dqc

%given r_ddqc (r_ti = 0)
    if ~isnan(r_ddqc)
        r_ddqc = sign(r_qf-r_qi)*r_ddqc;
        delta = (r_ddqc*(r_tf-r_ti)^2-4*(r_qf-r_qi))/r_ddqc;
        if delta >= 0
            r_tc = (r_tf-r_ti)/2 - 0.5 * sqrt(delta);
            if  r_tc <= (r_tf-r_ti)/2
                r_dqc = r_ddqc*r_tc;
            else
                fprintf("Error: tc > (r_tf-r_ti)/2\n");
            end
        else
            fprintf("Error: sqrt must be positive\n");
        end
    end
    
q(t) = piecewise((0<=t)&(t<=(ti + tc)),       qi + (dqc/(2*tc))*(t-ti).^2, ...
                ((ti + tc)<t)&(t<=(tf - tc)), qi + dqc*(t-ti-tc/2),  ...
                ((tf - tc)<=t)&(t<=tf),       qf - (dqc/(2*tc))*(tf - t).^2);
dq(t) = diff(q);
ddq(t) = diff(dq);


qt = subs(q(t), [ti,tf,qi,qf,tc,dqc,tc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_tc]);
dqt = subs(dq(t), [ti,tf,qi,qf,tc,dqc,tc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_tc]);
ddqt = subs(ddq(t), [ti,tf,qi,qf,tc,dqc,tc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_tc]);
    
end

