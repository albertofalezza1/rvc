function [q,v,a,r_t,r_tf] = trapezioMultiSmooth(r_qi, r_qo, r_qf, r_ti, r_maxAcc, r_maxVel)
%TRAPEZIOMULTI 
    sgn = 1;  r_dqi=0; r_dqf=0;
    if sign(r_qi - r_qo) ~= sign(r_qf - r_qi)
            r_dqk = 0;
    elseif sign(r_qi - r_qo) == sign(r_qf - r_qi)
            r_dqk = sign(r_qi - r_qo)*r_maxVel;
    end
    if r_maxVel == 0
        r_maxVel = nan;
    end

    if ~isnan(r_dqi) && ~isnan(r_dqf)
        if r_qf - r_qi < 0
            r_dqi = -r_dqi; r_dqf = -r_dqf; r_qi = -r_qi; r_qf = -r_qf; sgn = -1;
        end
        %given dqc & ddqc max
        if ~isnan(r_maxVel) && ~isnan(r_maxAcc)
            if r_maxAcc*(r_qf-r_qi) > abs(r_dqi^2 - r_dqf^2)*0.5 %Feasibility check
                if(r_maxAcc*(r_qf-r_qi) > (r_maxVel)^2-(r_dqi^2 - r_dqf^2)/2) %dqc max is reached
                    r_dqc = r_maxVel;
                    r_ta = (r_maxVel - r_dqi)/r_maxAcc; r_td = (r_maxVel - r_dqf)/r_maxAcc;
                    %we compute r_tf
                    r_tf =  r_ti+(r_qf-r_qi)/r_maxVel + r_maxVel/(2*r_maxAcc)*(1-r_dqi/r_maxVel)^2 + r_maxVel/(2*r_maxAcc)*(1-r_dqf/r_maxVel)^2;
                else %dqc max is not reached
                    r_dqc = sqrt(r_maxAcc*(r_qf-r_qi)+(r_dqi^2+r_dqf^2)/2);
                    %we compute r_tf
                    r_ta = (r_dqc-r_dqi)/r_maxAcc; r_td = (r_dqc-r_dqf)/r_maxAcc; r_tf = r_ta+r_td+r_ti;
                end
            else
                fprintf("ddqc max is not feasible\n");
            end
        end   
    end
    
    t1=linspace(r_ti,r_ta+r_ti,1000);
    if size(t1)== 1 %cover the case ta = 0
        t1=[];
    end
    t2=linspace(r_ta+r_ti,r_tf-r_td,1000);
    t3=linspace(r_tf-r_td,r_tf,1000);
    if size(t3) == 1 %cove the case td = 0
        t3 = [];
    end
    r_t = [t1 t2 t3];
    
    
    syms qi qf q(t) tf ti dqc dqi dqf ta td
    
    % acceleration phase t = t1
    aq(t) = qi + dqi*(t-ti)+((dqc-dqi)/(2*ta))*(t-ti).^2;
    daq(t) = diff(aq);
    ddaq(t) = diff(daq);
    % constant velocity phase t = t2
    vq(t) = qi+dqi*(ta/2)+dqc*(t-ti-ta/2);
    dvq(t) = diff(vq);
    ddvq(t) = diff(dvq);
    % deceleration phase t = t3
    sq(t) = qf-dqf*(tf-t)-(dqc-dqf)/(2*td)*(tf-t).^2;
    dsq(t) = diff(sq);
    ddsq(t) = diff(dsq);

    q1(t) = subs(aq(t), [qi,dqi,ti,dqc,ta], [r_qi,r_dqk,r_ti,r_dqc,r_ta]);
    v1(t) = subs(daq(t), [qi,dqi,ti,dqc,ta], [r_qi,r_dqk,r_ti,r_dqc,r_ta]);
    a1(t) = subs(ddaq(t), [qi,dqi,ti,dqc,ta], [r_qi,r_dqk,r_ti,r_dqc,r_ta]);

    q2(t) = subs(vq(t), [qi,dqi,ti,dqc,ta], [r_qi,r_dqk,r_ti,r_dqc,r_ta]);
    v2(t) = subs(dvq(t), [qi,dqi,ti,dqc,ta], [r_qi,r_dqk,r_ti,r_dqc,r_ta]);
    a2(t) = subs(ddvq(t), [qi,dqi,ti,dqc,ta], [r_qi,r_dqk,r_ti,r_dqc,r_ta]);

    q3(t) = subs(sq(t), [qf,dqf,tf,dqc,td], [r_qf,r_dqk,r_tf,r_dqc,r_td]);
    v3(t) = subs(dsq(t), [qf,dqf,tf,dqc,td], [r_qf,r_dqk,r_tf,r_dqc,r_td]);
    a3(t) = subs(ddsq(t), [qf,dqf,tf,dqc,td], [r_qf,r_dqk,r_tf,r_dqc,r_td]);
    
    q = sgn*[q1(t1) q2(t2) q3(t3)];
    v = sgn*[v1(t1) v2(t2) v3(t3)];
    a = sgn*[a1(t1) a2(t2) a3(t3)];
    
end

