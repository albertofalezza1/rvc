function [qt,dqt,ddqt, tf] = trapezioDQC_DDQC(r_ti, r_qi, r_qf, r_dqc, r_ddqc)
%TRAPEZIODQC_DDQC 
syms qi qf q(t) tf ti tc ddqc dqc

    if ~isnan(r_ddqc) && ~isnan(r_dqc)
        if r_ddqc ~=0 && r_dqc ~= 0
            r_ddqc = sign(r_qf-r_qi)*r_ddqc; r_dqc = sign(r_qf-r_qi)*r_dqc;
            r_tc = r_dqc/r_ddqc;
            r_tf =(r_dqc^2+r_ddqc*(r_qf-r_qi))/(r_dqc*r_ddqc)+r_ti;
        else
            fprintf("r_ddqc and r_dqc must be different than zero\n");
        end
    end
    
q(t) = piecewise((0<=t)&(t<=(ti + tc)),       qi + (dqc/(2*tc))*(t-ti).^2, ...
                ((ti + tc)<t)&(t<=(tf - tc)), qi + dqc*(t-ti-tc/2),  ...
                ((tf - tc)<=t)&(t<=tf),       qf - (dqc/(2*tc))*(tf - t).^2);      
dq(t) = diff(q);
ddq(t) = diff(dq);

qt = subs(q(t), [ti,tf,qi,qf,tc,dqc,ddqc,tc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_ddqc,r_tc]);
dqt = subs(dq(t), [ti,tf,qi,qf,tc,dqc,ddqc,tc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_ddqc,r_tc]);
ddqt = subs(ddq(t), [ti,tf,qi,qf,tc,dqc,ddqc,tc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_ddqc,r_tc]);
tf = r_tf;
end

