function [qt,dqt,ddqt] = trapezioDQC(r_ti, r_tf, r_qi, r_qf, r_dqc)
%TRAPEZIODQC
syms qi qf q(t) tf ti tc ddqc dqc

    if ~isnan(r_dqc)
        r_dqc = sign(r_qf-r_qi)*r_dqc;
        r_tc = (r_qi-r_qf+r_dqc*(r_tf-r_ti))/r_dqc;
        if r_tc > 0 && r_tc <= (r_tf-r_tc)
            r_ddqc = r_dqc^2/r_qi-r_qf+r_dqc*(r_tf-r_ti);
        else
            fprintf("Error: tc < 0 or tc > r_tf-tc\ntc = " + r_tc + "\ntf = " + r_tf + "\n");
        end
    end
    
q(t) = piecewise((0<=t)&(t<=(ti + tc)),       qi + (dqc/(2*tc))*(t-ti).^2, ...
                ((ti + tc)<t)&(t<=(tf - tc)), qi + dqc*(t-ti-tc/2),  ...
                ((tf - tc)<=t)&(t<=tf),       qf - (dqc/(2*tc))*(tf - t).^2);
dq(t) = diff(q);
ddq(t) = diff(dq);


qt = subs(q(t), [ti,tf,qi,qf,tc,dqc,ddqc,tc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_ddqc,r_tc]);
dqt = subs(dq(t), [ti,tf,qi,qf,tc,dqc,ddqc,tc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_ddqc,r_tc]);
ddqt = subs(ddq(t), [ti,tf,qi,qf,tc,dqc,ddqc,tc], [r_ti,r_tf,r_qi,r_qf,r_tc,r_dqc,r_ddqc,r_tc]);

end