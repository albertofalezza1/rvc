function [r_q, r_dq, r_ddq, r_dddq, r_ddddq] = i5thPoly(sqi,sqf,sdqi,sdqf,sddqi,sddqf,r_ti,r_tf)
%I5THPOLY

syms a5 a4 a3 a2 a1 a0 qi qf dqi dqf ddqi ddqf q(t) tf ti

q(t) = a5*t^5 + a4*t^4 + a3*t^3 + a2*t^2 + a1*t + a0;
dq(t) = diff(q);
ddq(t) = diff(dq);
dddq(t) = diff(ddq);
ddddq(t) = diff(dddq);

[sa0,sa1,sa2,sa3,sa4,sa5] = solve([q(ti) == qi, dq(ti) == dqi, ddq(ti) == ddqi, ...
                                   q(tf) == qf,  dq(tf) == dqf, ddq(tf) == ddqf], ...
                                   [a0, a1, a2, a3, a4, a5])

s_q(t) = subs(q(t), [a0,a1,a2,a3,a4,a5], [sa0, sa1, sa2, sa3, sa4, sa5])
s_dq(t) = subs(dq(t), [a0,a1,a2,a3,a4,a5], [sa0, sa1, sa2, sa3, sa4, sa5])
s_ddq(t) = subs(ddq(t), [a0,a1,a2,a3,a4,a5], [sa0, sa1, sa2, sa3, sa4, sa5])
s_dddq(t) = subs(dddq(t), [a0,a1,a2,a3,a4,a5], [sa0,sa1, sa2, sa3, sa4, sa5])
s_ddddq(t) = subs(ddddq(t), [a0,a1,a2,a3,a4,a5], [sa0,sa1, sa2, sa3, sa4, sa5])

r_q(t) = subs(s_q(t), [qi,qf,dqi,dqf,ddqi,ddqf,ti,tf], [sqi, sqf, sdqi, sdqf, sddqi, sddqf, r_ti, r_tf])
r_dq(t) = subs(s_dq(t), [qi,qf,dqi,dqf,ddqi,ddqf,ti,tf], [sqi, sqf, sdqi, sdqf, sddqi, sddqf, r_ti, r_tf])
r_ddq(t) = subs(s_ddq(t), [qi,qf,dqi,dqf,ddqi,ddqf,ti,tf], [sqi, sqf, sdqi, sdqf, sddqi, sddqf, r_ti, r_tf])
r_dddq(t) = subs(s_dddq(t), [qi,qf,dqi,dqf,ddqi,ddqf,ti,tf], [sqi,sqf, sdqi, sdqf, sddqi, sddqf, r_ti, r_tf])
r_ddddq(t) = subs(s_ddddq(t), [qi,qf,dqi,dqf,ddqi,ddqf,ti,tf], [sqi,sqf, sdqi, sdqf, sddqi, sddqf, r_ti, r_tf])

r_q = r_q(t);
r_dq = r_dq(t);
r_ddq = r_ddq(t);
r_dddq = r_dddq(t);
r_ddddq = r_ddddq(t);
end