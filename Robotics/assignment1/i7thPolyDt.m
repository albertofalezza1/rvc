function [r_q, r_dq, r_ddq, r_dddq, r_ddddq] = i7thPolyDt(sqi,sqf,sdqi,sdqf,sddqi,sddqf,sdddqi,sdddqf,r_ti,r_tf)
%I7THPOLYDT 

syms a7 a6 a5 a4 a3 a2 a1 a0 qi qf dqi dqf ddqi ddqf dddqi dddqf q(t) tf ti

q(t) = a7*(t-ti)^7 + a6*(t-ti)^6 + a5*(t-ti)^5 + a4*(t-ti)^4 + a3*(t-ti)^3 + a2*(t-ti)^2 + a1*(t-ti) + a0;
dq(t) = diff(q);
ddq(t) = diff(dq);
dddq(t) = diff(ddq);
ddddq(t) = diff(dddq);

[sa0,sa1,sa2,sa3,sa4,sa5,sa6,sa7] = solve([q(ti) == qi, dq(ti) == dqi, ddq(ti) == ddqi, dddq(ti) == dddqi...
                                           q(tf) == qf,  dq(tf) == dqf, ddq(tf) == ddqf, dddq(tf) == dddqf], ...
                                           [a0, a1, a2, a3, a4, a5, a6, a7])

s_q(t) = subs(q(t), [a0,a1,a2,a3,a4,a5,a6,a7], [sa0, sa1, sa2, sa3, sa4, sa5, sa6, sa7])
s_dq(t) = subs(dq(t), [a0,a1,a2,a3,a4,a5,a6,a7], [sa0, sa1, sa2, sa3, sa4, sa5, sa6, sa7])
s_ddq(t) = subs(ddq(t), [a0,a1,a2,a3,a4,a5,a6,a7], [sa0, sa1, sa2, sa3, sa4, sa5, sa6, sa7])
s_dddq(t) = subs(dddq(t), [a0,a1,a2,a3,a4,a5,a6,a7], [sa0,sa1, sa2, sa3, sa4, sa5, sa6, sa7])
s_ddddq(t) = subs(ddddq(t), [a0,a1,a2,a3,a4,a5,a6,a7], [sa0,sa1, sa2, sa3, sa4, sa5, sa6, sa7])

r_q(t) = subs(s_q(t), [qi,qf,dqi,dqf,ddqi,ddqf,dddqi,dddqf,ti,tf], [sqi, sqf, sdqi, sdqf, sddqi, sddqf, sdddqi, sdddqf, r_ti, r_tf])
r_dq(t) = subs(s_dq(t), [qi,qf,dqi,dqf,ddqi,ddqf,dddqi,dddqf,ti,tf], [sqi, sqf, sdqi, sdqf, sddqi, sddqf, sdddqi, sdddqf, r_ti, r_tf])
r_ddq(t) = subs(s_ddq(t), [qi,qf,dqi,dqf,ddqi,ddqf,dddqi,dddqf,ti,tf], [sqi, sqf, sdqi, sdqf, sddqi, sddqf, sdddqi, sdddqf, r_ti, r_tf])
r_dddq(t) = subs(s_dddq(t), [qi,qf,dqi,dqf,ddqi,ddqf,dddqi,dddqf,ti,tf], [sqi,sqf, sdqi, sdqf, sddqi, sddqf, sdddqi, sdddqf, r_ti, r_tf])
r_ddddq(t) = subs(s_ddddq(t), [qi,qf,dqi,dqf,ddqi,ddqf,dddqi,dddqf,ti,tf], [sqi,sqf, sdqi, sdqf, sddqi, sddqf, sdddqi, sdddqf, r_ti, r_tf])

r_q = r_q(t);
r_dq = r_dq(t);
r_ddq = r_ddq(t);
r_dddq = r_dddq(t);
r_ddddq = r_ddddq(t);
end