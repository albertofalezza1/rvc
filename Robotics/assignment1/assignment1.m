% Implement in Matlab 3rd– (cubic), 5th–, 7th–order polynomials 
% for qi > qf and qi < qf , and in both formulations
close all;
clear;

sqi = 10;    %initial position
sqf = 0;   %final position
sdqi = 0;   %initial velocity
sdqf = 0;   %final velocity
sddqi = 0;  %initial acceleration
sddqf = 0;  %final acceleration
sdddqi = 0; %initial jerk
sdddqf = 0; %final jerk
r_ti = 0;   %initial time
r_tf = 8;   %final time

%% Cubic polynomials
syms q(t)

[r_q(t), r_dq(t), r_ddq(t), r_dddq(t), r_ddddq(t)] = CubicPoly(sqi, sqf, sdqi, sdqf, r_ti, r_tf);

%% Cubic polynomials with Δt
syms q(t)

[r_q(t), r_dq(t), r_ddq(t), r_dddq(t), r_ddddq(t)] = CubicPolyDt(sqi, sqf, sdqi, sdqf, r_ti, r_tf);

%% 5th-order Polynomials
syms q(t)

[r_q(t), r_dq(t), r_ddq(t), r_dddq(t), r_ddddq(t)] = i5thPoly(sqi, sqf, sdqi, sdqf, sddqi, sddqf, r_ti, r_tf);

%% 5th-order Polynomials with Δt
syms q(t)

[r_q(t), r_dq(t), r_ddq(t), r_dddq(t), r_ddddq(t)] = i5thPolyDt(sqi, sqf, sdqi, sdqf, sddqi, sddqf, r_ti, r_tf);

%% 7th-order Polynomials
syms q(t)

[r_q(t), r_dq(t), r_ddq(t), r_dddq(t), r_ddddq(t)] = i7thPoly(sqi, sqf, sdqi, sdqf, sddqi, sddqf, sdddqi, sdddqf, r_ti, r_tf);

%% 7th-order Polynomials with Δt
syms q(t)

[r_q(t), r_dq(t), r_ddq(t), r_dddq(t), r_ddddq(t)] = i7thPolyDt(sqi, sqf, sdqi, sdqf, sddqi, sddqf, sdddqi, sdddqf, r_ti, r_tf);

%% Plot

t = r_ti:0.001:r_tf;

position = subplot(5,1,1);
plot(t, r_q(t))
title('position');
velocity = subplot(5,1,2);
plot(t, r_dq(t))
title('velocity');
acceleration = subplot(5,1,3);
plot(t, r_ddq(t))
title('acceleration');
jerk = subplot(5,1,4); 
plot(t, r_dddq(t))
title('jerk');
snap = subplot(5,1,5); 
plot(t, r_ddddq(t))
title('snap');