%% Assignment 5
% Compute the 3D trajectory, also with velocity and acceleration, in the
% picture as a combination of linear and circular motion primitives and
% compare it with the trajectory obtained using one of the multi-point
% methods

close all;
clear; 
clc;
%% Points
p1 = [0;0;0];
p2 = [1;0;0];
p3 = [2;1;0];
p4 = [2;1;2];
p5 = [2;0;2];

points = [p1 p2 p3 p4 p5]
%% Paths Forward
[pos1, vel1, acc1, jer1] = rectilinear_path(p1, p2);
[pos4, vel4, acc4, jer4]  = rectilinear_path(p4, p5);
r = [0; 0; 1]; % unit vector to orient the circle
d = [1; 1; 1]; % d is a point along the circle origin
[pos2, vel2, acc2, jer2]  = circular_path(r, d, p2, p3, pi/2);
r = [1; 0; 0]; % unit vector to orient the circle
[pos3, vel3, acc3, jer3]  = circular_path(r, d, p3, p4, pi);


pos = [pos1 pos2 pos3 pos4];
vel = [vel1 vel2 vel3 vel4];
acc = [acc1 acc2 acc3 acc4];
jer = [jer1 jer2 jer3 jer4];

%% Plots
sgtitle('Operational space trajectory - Forward')
subplot(2,2,1)
plot3(pos(1,:),pos(2,:),pos(3,:));
hold on
% centers of the circular paths
plot3(1,1,0,'*','Color','magenta');
plot3(2,1,1,'*','Color','magenta');
% Points to follow
plot3(p2(1),p2(2),p2(3),'*','Color','black');
plot3(p3(1),p3(2),p3(3),'*','Color','black');
plot3(p4(1),p4(2),p4(3),'*','Color','black');
plot3(pos(1,1),pos(2,1),pos(3,1),'*','Color','g');
plot3(pos(1,end),pos(2,end),pos(3,end),'*','Color','r');
% Settings
title('Position')
xlabel('x');
ylabel('y');
zlabel('z');
grid on;

subplot(2,2,2)
plot3(vel(1,:),vel(2,:),vel(3,:));
hold on
plot3(vel(1,1),vel(2,1),vel(3,1),'*','Color','g');
plot3(vel(1,end),vel(2,end),vel(3,end),'*','Color','r');
title('Velocity')
xlabel('x');
ylabel('y');
zlabel('z');
grid on;

subplot(2,2,3)
plot3(acc(1,:),acc(2,:),acc(3,:));
hold on
plot3(acc(1,1),acc(2,1),acc(3,1),'*','Color','g');
plot3(acc(1,end),acc(2,end),acc(3,end),'*','Color','r');
title('Acceleration')
xlabel('x');
ylabel('y');
zlabel('z');
grid on;

subplot(2,2,4)
plot3(jer(1,:),jer(2,:),jer(3,:));
hold on
plot3(jer(1,1),jer(2,1),jer(3,1),'*','Color','g');
plot3(jer(1,end),jer(2,end),jer(3,end),'*','Color','r');
title('Jerk')
xlabel('x');
ylabel('y');
zlabel('z');
grid on;
%% Inverted Paths
[pos1, vel1, acc1, jer1] = rectilinear_path(p5, p4);
[pos4, vel4, acc4, jer4]  = rectilinear_path(p2, p1);
r = [0; 0; -1]; % unit vector to orient the circle
d = [1; 1; 1]; % d is a point along the circle origin
[pos3, vel3, acc3, jer3]  = circular_path(r, d, p3, p2, pi/2);
r = [-1; 0; 0]; % unit vector to orient the circle
[pos2, vel2, acc2, jer2]  = circular_path(r, d, p4, p3, pi);


pos = [pos1 pos2 pos3 pos4];
vel = [vel1 vel2 vel3 vel4];
acc = [acc1 acc2 acc3 acc4];
jer = [jer1 jer2 jer3 jer4];

%% Plots
figure;
sgtitle('Operational space trajectory - Backward')
subplot(2,2,1)
plot3(pos(1,:),pos(2,:),pos(3,:));
hold on
% centers of the circular paths
plot3(1,1,0,'*','Color','magenta');
plot3(2,1,1,'*','Color','magenta');
% Points to follow
plot3(p2(1),p2(2),p2(3),'*','Color','black');
plot3(p3(1),p3(2),p3(3),'*','Color','black');
plot3(p4(1),p4(2),p4(3),'*','Color','black');
plot3(pos(1,1),pos(2,1),pos(3,1),'*','Color','g');
plot3(pos(1,end),pos(2,end),pos(3,end),'*','Color','r');
title('Position')
xlabel('x');
ylabel('y');
zlabel('z');
grid on;

subplot(2,2,2)
plot3(vel(1,:),vel(2,:),vel(3,:));
hold on
plot3(vel(1,1),vel(2,1),vel(3,1),'*','Color','g');
plot3(vel(1,end),vel(2,end),vel(3,end),'*','Color','r');
title('Velocity')
xlabel('x');
ylabel('y');
zlabel('z');
grid on;

subplot(2,2,3)
plot3(acc(1,:),acc(2,:),acc(3,:));
hold on
plot3(acc(1,1),acc(2,1),acc(3,1),'*','Color','g');
plot3(acc(1,end),acc(2,end),acc(3,end),'*','Color','r');
title('Acceleration')
xlabel('x');
ylabel('y');
zlabel('z');
grid on;

subplot(2,2,4)
plot3(jer(1,:),jer(2,:),jer(3,:));
hold on
plot3(jer(1,1),jer(2,1),jer(3,1),'*','Color','g');
plot3(jer(1,end),jer(2,end),jer(3,end),'*','Color','r');
title('Jerk')
xlabel('x');
ylabel('y');
zlabel('z');
grid on;
