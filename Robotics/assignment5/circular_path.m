function [p, vel, acc, jer] = circular_path(r, d, pi, pf, theta)
% circular path computes a circular path given the unit vector r, the
% position d of a point along the circle axis and the position vector pi
    [c, ro] = circle(r, d, pi);
    arc = ro*theta;
    s = 0:0.01:arc;

    pp = [ro*cos(s/ro); 
          ro*sin(s/ro); 
          zeros([1 size(s, 2)])
          ];
    
    xp = (pi - c) / norm(pi - c);
    zp = r / norm(r);
    yp = cross(zp, xp) / norm(cross(zp, xp));
    
    R = [xp yp zp];
    p = c + R*pp;
    vel = R * [-sin(s/ro); cos(s/ro); zeros([1 size(s, 2)])];
    acc = R * [-cos(s/ro); -sin(s/ro); zeros([1 size(s, 2)])];
    jer = R * [sin(s/ro); -cos(s/ro); zeros([1 size(s, 2)])];
end

