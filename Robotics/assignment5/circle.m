function [c, ro] = circle(r, d, pi)
% circle compute the center and the radius of a circle
    delta = pi - d;
    c = d + (delta.'*r)*r;
    ro = norm(pi - c);    
end

