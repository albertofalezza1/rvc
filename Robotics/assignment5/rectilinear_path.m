function [p, vel, acc, jer] = rectilinear_path(pi, pf)
    % rectilinear_path computes the motion primitives as a line between pi and
    % pf which are coordinates in the 3D space of the starting point and final point to follow
    L = norm(pf - pi);    
    s = 0:0.01:L;
    p = pi + s .* ((pf - pi) / L);
    t = (pf - pi)/L;
    ddp = [0; 0; 0];
    dddp = [0; 0; 0];
    vel = [];
    acc = [];
    jer = [];
    for i=1:size(p,2)
        vel = [vel t];
        acc = [acc ddp];
        jer = [jer dddp];
    end
end

