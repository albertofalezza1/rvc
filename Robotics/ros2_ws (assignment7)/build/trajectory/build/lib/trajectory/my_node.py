from multiprocessing.connection import wait
from turtle import position
import numpy as np
import math
import rclpy
from rclpy.node import Node

from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Bool

from scipy.spatial.transform import Rotation as R

import threading
import sys

import warnings
warnings.filterwarnings("ignore")

class Trajectory(Node):

    def __init__(self):

        super().__init__('trajectory') # node name = trajectory
        
        ee_target_topic = '/ur5/ee_target/pose' # topic on which we public the target pose for the end effector
        ee_gripper_topic = '/wsg_50/controller/command' # topic to publish if we want to close/open the gripper
        self.ee_target_pub = self.create_publisher(PoseStamped, ee_target_topic, 10) 
        self.ee_gripper_pub = self.create_publisher(Bool, ee_gripper_topic, 10)

        self.queue = 10

        green_cube_topic = '/cube_green/pose'
        dest_green_cube_topic = '/dest_cube_green/pose'
        self.read_green_cube = False
        self.read_dest_green_cube = False
        self.green_cube_sub = self.create_subscription(
            PoseStamped,
            green_cube_topic,
            self.green_cube_callback,
            self.queue)
        self.green_cube_sub  # prevent unused variable warning

        self.dest_green_cube_sub = self.create_subscription(
            PoseStamped,
            dest_green_cube_topic,
            self.dest_green_cube_callback,
            self.queue)
        self.dest_green_cube_sub  # prevent unused variable warning

        red_cube_topic = '/cube_red/pose'
        dest_red_cube_topic = '/dest_cube_red/pose'
        self.read_red_cube = False
        self.read_dest_red_cube = False
        self.red_cube_sub = self.create_subscription(
            PoseStamped,
            red_cube_topic,
            self.red_cube_callback,
            self.queue)
        self.red_cube_sub  # prevent unused variable warning

        self.dest_red_cube_sub = self.create_subscription(
            PoseStamped,
            dest_red_cube_topic,
            self.dest_red_cube_callback,
            self.queue)
        self.dest_red_cube_sub  # prevent unused variable warning


        yellow_cube_topic = '/cube_yellow/pose'
        dest_yellow_cube_topic = '/dest_cube_yellow/pose'
        self.read_yellow_cube = False
        self.read_dest_yellow_cube = False
        self.yellow_cube_sub = self.create_subscription(
            PoseStamped,
            yellow_cube_topic,
            self.yellow_cube_callback,
            self.queue)
        self.yellow_cube_sub  # prevent unused variable warning

        self.dest_yellow_cube_sub = self.create_subscription(
            PoseStamped,
            dest_yellow_cube_topic,
            self.dest_yellow_cube_callback,
            self.queue)
        self.dest_yellow_cube_sub  # prevent unused variable warning


        blu_cube_topic = '/cube_blu/pose'
        dest_blu_cube_topic = '/dest_cube_blu/pose'
        self.read_blu_cube = False
        self.read_dest_blu_cube = False
        self.blu_cube_sub = self.create_subscription(
            PoseStamped,
            blu_cube_topic,
            self.blu_cube_callback,
            self.queue)
        self.blu_cube_sub  # prevent unused variable warning

        self.dest_blu_cube_sub = self.create_subscription(
            PoseStamped,
            dest_blu_cube_topic,
            self.dest_blu_cube_callback,
            self.queue)
        self.dest_blu_cube_sub  # prevent unused variable warning


        self.step = 0.1 # call the callback each step seconds
        self.timer = self.create_timer(self.step, self.timer_callback) # call the callback function every 2 seconds

        self.waypoints = {
            'setup' : {
                'positions' : [
                    [0.48448035246821225, 0.1284394284202057, 0.6094724182801186],
                    [0.5000293654092437, -0.12627595948539352, 0.4366663150573229],
                    [0.512631299671664, 0.0564281384813316, 0.4366663150573229],
                    [0.32258434008200254, 0.40238579466649493, 0.4366663150573229],
                    [0.4393937874564731, 0.3012380293055022, 0.4700098735989296]
                ],
                'orientations':[
                    [0.5008981852081547, 0.4979430153488416, 0.5013498603586369, 0.4998020398506354], # ee initial orientation
                    [0.8588917179441309, 0.5069989346092104, 0.03832755414932206, 0.061547508032705436],
                    [0.7552256739546912, 0.6514423107594236, 0.048646532877060174, 0.053764411938997905],
                    [0.44844135338926583, 0.8908665755422068, 0.06551828609203064, 0.031055616877917264],
                    [0.5590389616912609, 0.8011298275897255, 0.17639923479787167, 0.12062233880977127]
                ],
                'time' : [0, 1, 2, 3, 4], # Times,
                'gripper_status' : [0, 0, 0, 0, 0] # all orientations
            },
            'green' : {
                'positions' : [
                            [0.4872189891362343, 0.12886971084061952, 0.4222054451023206],  # ee middle point to read green aruco
                            [0.4174157132204117, 0.2931969869549421, 0.4292194705446947], # second middle point 
                        ], # all positions
                'orientations' :  [
                            [0.708191965261188, 0.7050561590019159, 0.027156008591312335, 0.0249500338747294], # ee middle point orientation
                            [0.5717546636423504, 0.8195954194663739, 0.031154490812532038, 0.019731971396022025], # second middle point orientation
                        ],
                'time' : [0, 2, 4, 6 ,8, 10, 12], # Times,
                'gripper_status' : [0, 0, 1, 1, 1, 1, 0] # all orientations
            },
            'red' : {
                'positions' : [
                    # Insert green destination at runtime as first position
                    [0.4738226957326547, -0.1512842167067881, 0.41566477578730143], # EE middle point
                    # Insert red cube position
                    [0.4962137587268221, 0.2013331065610748, 0.47902232629835506], # EE 2nd middle point
                    # Insert red cube destination at run time
                ], # all positions
                'orientations' :  [
                    [0.878188713109899, 0.4770630291202459, -0.015183374805682355, -0.031063089712799787],
                    [0.6360767828841252, 0.7305407993967807, 0.18873439222081037, 0.16154193228241365],
                ],
                'time' : [0, 3, 6, 9, 12, 15, 18], # Times,
                'gripper_status' : [0, 0, 1, 1, 1, 1, 0] # all orientations
            },
            'yellow' : {
                'positions' : [
                    [0.5091039714381875, 0.08861268461346315, 0.45248511869461167],
                    [0.5091039714381875, 0.08861268461346315, 0.45248511869461167],
                ], # all positions
                'orientations' :  [
                    [0.7070224588214137, 0.661160711290053, -0.04936110879603444, 0.24606754663502403],
                    [0.7070224588214137, 0.661160711290053, -0.04936110879603444, 0.24606754663502403],
                ],
                'time' : [0, 3, 6, 9, 12, 15, 18], # Times,
                'gripper_status' : [0, 1, 1, 1, 1, 1, 0] # all orientations
            },
            'blu' : {
                'positions' : [
                    [0.5198103518640936, 0.12868824326018485, 0.4789851583750615],
                    [0.5198103518640936, 0.12868824326018485, 0.4789851583750615],
                ], # all positions
                'orientations' :  [
                    [0.6864370560345473, 0.683449623197789, 0.17668451246030076, 0.17459485590959026],
                    [0.6864370560345473, 0.683449623197789, 0.17668451246030076, 0.17459485590959026],
                ],
                'time' : [0, 3, 6, 9, 12, 15, 18], # Times,
                'gripper_status' : [0, 1, 1, 1, 1, 1, 0] # all orientations
            }
        }

        # function to start the keyboard input
        self.startThread()
        self.unlockedPath1 = False
        self.unlockedPath2 = False
        self.countDestRead = 0
        self.orderDict = {}
        self.actual_waypoint = 'none'

    def green_cube_callback(self, green_pose_msg):
        if not self.read_green_cube:
            # self.get_logger().info("GREEN CUBE")
            new_position = np.array([green_pose_msg.pose.position.x, green_pose_msg.pose.position.y, green_pose_msg.pose.position.z])
            new_orientation = np.array([green_pose_msg.pose.orientation.x, green_pose_msg.pose.orientation.y, green_pose_msg.pose.orientation.z, green_pose_msg.pose.orientation.w])
            self.waypoints['green']['positions'] = np.insert(self.waypoints['green']['positions'], 2, new_position, 0)
            self.waypoints['green']['positions'] = np.insert(self.waypoints['green']['positions'], 2, new_position, 0)
            self.waypoints['green']['orientations'] = np.insert(self.waypoints['green']['orientations'], 2, new_orientation, 0)
            self.waypoints['green']['orientations'] = np.insert(self.waypoints['green']['orientations'], 2, new_orientation, 0)
            # self.get_logger().info("GREEN CUBE p:" + str(new_position))
            # self.get_logger().info("GREEN CUBE r:" + str(new_orientation))
        self.read_green_cube = True

    def dest_green_cube_callback(self, dest_green_pose_msg):
        if not self.read_dest_green_cube:
            # self.get_logger().info("DESTINATION GREEN CUBE")
            new_position = np.array([dest_green_pose_msg.pose.position.x, dest_green_pose_msg.pose.position.y, dest_green_pose_msg.pose.position.z])
            new_orientation = np.array([dest_green_pose_msg.pose.orientation.x, dest_green_pose_msg.pose.orientation.y, dest_green_pose_msg.pose.orientation.z, dest_green_pose_msg.pose.orientation.w])
            self.waypoints['green']['positions'] = np.insert(self.waypoints['green']['positions'], len(self.waypoints['green']['positions']), new_position, 0)
            self.waypoints['green']['orientations'] = np.insert(self.waypoints['green']['orientations'], len(self.waypoints['green']['orientations']), new_orientation, 0)
            self.waypoints['green']['positions'] = np.insert(self.waypoints['green']['positions'], len(self.waypoints['green']['positions']), new_position, 0)
            self.waypoints['green']['orientations'] = np.insert(self.waypoints['green']['orientations'], len(self.waypoints['green']['orientations']), new_orientation, 0)
            # set the start for next waypoint to the end of green position if exists
            self.mobility_order('green', new_position, new_orientation)
        self.read_dest_green_cube = True

    def red_cube_callback(self, red_pose_msg):
        if not self.read_red_cube:
            # self.get_logger().info("RED CUBE")
            new_position = np.array([red_pose_msg.pose.position.x, red_pose_msg.pose.position.y, red_pose_msg.pose.position.z])
            new_orientation = np.array([red_pose_msg.pose.orientation.x, red_pose_msg.pose.orientation.y, red_pose_msg.pose.orientation.z, red_pose_msg.pose.orientation.w])
            self.waypoints['red']['positions'] = np.insert(self.waypoints['red']['positions'], 1, new_position, 0)
            self.waypoints['red']['positions'] = np.insert(self.waypoints['red']['positions'], 1, new_position, 0)
            self.waypoints['red']['orientations'] = np.insert(self.waypoints['red']['orientations'], 1, new_orientation, 0)
            self.waypoints['red']['orientations'] = np.insert(self.waypoints['red']['orientations'], 1, new_orientation, 0)
            # self.get_logger().info("RED CUBE p:" + str(new_position))
            # self.get_logger().info("RED CUBE r:" + str(new_orientation))
        self.read_red_cube = True

    def dest_red_cube_callback(self, dest_red_pose_msg):
        if not self.read_dest_red_cube:
            # self.get_logger().info("DESTINATION RED CUBE")
            new_position = np.array([dest_red_pose_msg.pose.position.x, dest_red_pose_msg.pose.position.y, dest_red_pose_msg.pose.position.z])
            new_orientation = np.array([dest_red_pose_msg.pose.orientation.x, dest_red_pose_msg.pose.orientation.y, dest_red_pose_msg.pose.orientation.z, dest_red_pose_msg.pose.orientation.w])
            self.waypoints['red']['positions'] = np.insert(self.waypoints['red']['positions'], len(self.waypoints['red']['positions']), new_position, 0)
            self.waypoints['red']['orientations'] = np.insert(self.waypoints['red']['orientations'], len(self.waypoints['red']['orientations']), new_orientation, 0)
            self.waypoints['red']['positions'] = np.insert(self.waypoints['red']['positions'], len(self.waypoints['red']['positions']), new_position, 0)
            self.waypoints['red']['orientations'] = np.insert(self.waypoints['red']['orientations'], len(self.waypoints['red']['orientations']), new_orientation, 0)
            # set the start for next waypoint to the end of red position if exists
            self.mobility_order('red', new_position, new_orientation)
        self.read_dest_red_cube = True

    def yellow_cube_callback(self, yellow_pose_msg):
        if not self.read_yellow_cube:
            # self.get_logger().info("YELLOW CUBE")
            new_position = np.array([yellow_pose_msg.pose.position.x, yellow_pose_msg.pose.position.y, yellow_pose_msg.pose.position.z])
            new_orientation = np.array([yellow_pose_msg.pose.orientation.x, yellow_pose_msg.pose.orientation.y, yellow_pose_msg.pose.orientation.z, yellow_pose_msg.pose.orientation.w])
            self.waypoints['yellow']['positions'] = np.insert(self.waypoints['yellow']['positions'], 1, new_position, 0)
            self.waypoints['yellow']['positions'] = np.insert(self.waypoints['yellow']['positions'], 1, new_position, 0)
            self.waypoints['yellow']['orientations'] = np.insert(self.waypoints['yellow']['orientations'], 1, new_orientation, 0)
            self.waypoints['yellow']['orientations'] = np.insert(self.waypoints['yellow']['orientations'], 1, new_orientation, 0)
            # self.get_logger().info("YELLOW CUBE p:" + str(new_position))
            # self.get_logger().info("YELLOW CUBE r:" + str(new_orientation))
        self.read_yellow_cube = True

    def dest_yellow_cube_callback(self, dest_yellow_pose_msg):
        if not self.read_dest_yellow_cube:
            # self.get_logger().info("DESTINATION YELLOW CUBE")
            new_position = np.array([dest_yellow_pose_msg.pose.position.x, dest_yellow_pose_msg.pose.position.y, dest_yellow_pose_msg.pose.position.z])
            new_orientation = np.array([dest_yellow_pose_msg.pose.orientation.x, dest_yellow_pose_msg.pose.orientation.y, dest_yellow_pose_msg.pose.orientation.z, dest_yellow_pose_msg.pose.orientation.w])
            self.waypoints['yellow']['positions'] = np.insert(self.waypoints['yellow']['positions'], len(self.waypoints['yellow']['positions']), new_position, 0)
            self.waypoints['yellow']['orientations'] = np.insert(self.waypoints['yellow']['orientations'], len(self.waypoints['yellow']['orientations']), new_orientation, 0)
            self.waypoints['yellow']['positions'] = np.insert(self.waypoints['yellow']['positions'], len(self.waypoints['yellow']['positions']), new_position, 0)
            self.waypoints['yellow']['orientations'] = np.insert(self.waypoints['yellow']['orientations'], len(self.waypoints['yellow']['orientations']), new_orientation, 0)
            # set the start for next waypoint to the end of yellow position if exists
            self.mobility_order('yellow', new_position, new_orientation)
        self.read_dest_yellow_cube = True


    def blu_cube_callback(self, blu_pose_msg):
        if not self.read_blu_cube:
            # self.get_logger().info("BLU CUBE")
            new_position = np.array([blu_pose_msg.pose.position.x, blu_pose_msg.pose.position.y, blu_pose_msg.pose.position.z])
            new_orientation = np.array([blu_pose_msg.pose.orientation.x, blu_pose_msg.pose.orientation.y, blu_pose_msg.pose.orientation.z, blu_pose_msg.pose.orientation.w])
            self.waypoints['blu']['positions'] = np.insert(self.waypoints['blu']['positions'], 1, new_position, 0)
            self.waypoints['blu']['positions'] = np.insert(self.waypoints['blu']['positions'], 1, new_position, 0)
            self.waypoints['blu']['orientations'] = np.insert(self.waypoints['blu']['orientations'], 1, new_orientation, 0)
            self.waypoints['blu']['orientations'] = np.insert(self.waypoints['blu']['orientations'], 1, new_orientation, 0)
            # self.get_logger().info("BLU CUBE p:" + str(new_position))
            # self.get_logger().info("BLU CUBE r:" + str(new_orientation))
        self.read_blu_cube = True

    def dest_blu_cube_callback(self, dest_blu_pose_msg):
        if not self.read_dest_blu_cube:
            # self.get_logger().info("DESTINATION BLU CUBE")
            new_position = np.array([dest_blu_pose_msg.pose.position.x, dest_blu_pose_msg.pose.position.y, dest_blu_pose_msg.pose.position.z])
            new_orientation = np.array([dest_blu_pose_msg.pose.orientation.x, dest_blu_pose_msg.pose.orientation.y, dest_blu_pose_msg.pose.orientation.z, dest_blu_pose_msg.pose.orientation.w])
            self.waypoints['blu']['positions'] = np.insert(self.waypoints['blu']['positions'], len(self.waypoints['blu']['positions']), new_position, 0)
            self.waypoints['blu']['orientations'] = np.insert(self.waypoints['blu']['orientations'], len(self.waypoints['blu']['orientations']), new_orientation, 0)
            self.waypoints['blu']['positions'] = np.insert(self.waypoints['blu']['positions'], len(self.waypoints['blu']['positions']), new_position, 0)
            self.waypoints['blu']['orientations'] = np.insert(self.waypoints['blu']['orientations'], len(self.waypoints['blu']['orientations']), new_orientation, 0)
            # set the start for next waypoint to the end of blue position if exists
            self.mobility_order('blu', new_position, new_orientation)
        self.read_dest_blu_cube = True

    def mobility_order(self, name, pos, ori):
        
        self.orderDict[name]['positions'] = pos
        self.orderDict[name]['orientations'] = ori
        self.countDestRead = self.countDestRead + 1
        if(self.countDestRead >= 4):
            for index, key in enumerate(reversed(self.waypoints), start = 1):
                if index >= 4: break
                i = -index-1
                # self.get_logger().info("End position: " + str(list(self.orderDict.keys())[i]) + " messa in inizio di: " + str(key))
                new_position = self.orderDict[list(self.orderDict.keys())[i]]['positions']
                new_orientation = self.orderDict[list(self.orderDict.keys())[i]]['orientations']
                self.waypoints[key]['positions'] = np.insert(self.waypoints[key]['positions'], 0, new_position, 0)
                self.waypoints[key]['orientations'] = np.insert(self.waypoints[key]['orientations'], 0, new_orientation, 0)
            
            # put the end position of the setup as a start of the first iteration
            new_position = [0.4393937874564731, 0.3012380293055022, 0.4700098735989296]
            new_orientation = [0.5590389616912609, 0.8011298275897255, 0.17639923479787167, 0.12062233880977127]
            self.waypoints[list(self.waypoints.keys())[1]]['positions'] = np.insert(self.waypoints[list(self.waypoints.keys())[1]]['positions'], 0, new_position, 0)
            self.waypoints[list(self.waypoints.keys())[1]]['orientations'] = np.insert(self.waypoints[list(self.waypoints.keys())[1]]['orientations'], 0, new_orientation, 0)

            # put deafult position the robot at the end of all iterations
            default_position = [0.4844807508278939, 0.12843272025495, 0.609472311195742]
            default_orientation = [0.5008983475926878, 0.4979418783560223, 0.5013215009991848, 0.4998314553868295]
            self.waypoints[list(self.waypoints.keys())[4]]['positions'] = np.insert(self.waypoints[list(self.waypoints.keys())[4]]['positions'], len(self.waypoints[list(self.waypoints.keys())[4]]['positions']), default_position, 0)
            self.waypoints[list(self.waypoints.keys())[4]]['orientations'] = np.insert(self.waypoints[list(self.waypoints.keys())[4]]['orientations'], len(self.waypoints[list(self.waypoints.keys())[4]]['orientations']), default_orientation, 0)
            self.waypoints[list(self.waypoints.keys())[4]]['time'] = np.insert(self.waypoints[list(self.waypoints.keys())[4]]['time'], len(self.waypoints[list(self.waypoints.keys())[4]]['time']), self.waypoints[list(self.waypoints.keys())[4]]['time'][-1]+3, 0)
            self.waypoints[list(self.waypoints.keys())[4]]['gripper_status'] = np.insert(self.waypoints[list(self.waypoints.keys())[4]]['gripper_status'], len(self.waypoints[list(self.waypoints.keys())[4]]['gripper_status']), 0, 0)

            self.unlockedPath1 = True

    def timer_callback(self):
        '''
            Timer called to compute position and orientation of the end effector. 
        '''
        if(self.actual_waypoint == 'setup' or (self.unlockedPath1 and self.unlockedPath2)):
            pose_msg = PoseStamped()
            gripper_msg = Bool()
            # self.get_logger().info('ACTUAL POINT : ' + str(self.actual_waypoint))
            # self.get_logger().info('COUNTER : ' + str(self.counter))
            # self.get_logger().info('POSITIONS LENGTH : ' + str(len(self.waypoints[self.actual_waypoint]['positions'])))
            
            positions = self.waypoints[self.actual_waypoint]['positions']
            orientations = self.waypoints[self.actual_waypoint]['orientations']
            
            pi = positions[self.counter - 1]
            pf = positions[self.counter]

            quat_i = orientations[self.counter - 1]
            quat_f = orientations[self.counter]

            # self.get_logger().info('\nActual Point: ' + str(self.actual_waypoint))
            # self.get_logger().info('\nCounter: ' + str(self.counter))

            if self.t <= self.tf:

                pe = self.compute_ee_position(pi, pf)
                phi_ee = self.compute_ee_orientation(quat_i, quat_f)

                # Position EE
                pose_msg.pose.position.x = pe[0]
                pose_msg.pose.position.y = pe[1]
                pose_msg.pose.position.z = pe[2]

                # Orientation EE
                pose_msg.pose.orientation.x = phi_ee[0]
                pose_msg.pose.orientation.y = phi_ee[1]
                pose_msg.pose.orientation.z = phi_ee[2]
                pose_msg.pose.orientation.w = phi_ee[3]

                self.ee_target_pub.publish(pose_msg)
                self.t += self.step
                
            else:
                # self.get_logger().info("Gripper Status: " + str(self.waypoints['green']['gripper_status'][self.counter]))
                if (self.waypoints[self.actual_waypoint]['gripper_status'][self.counter]):
                    gripper_msg.data = True
                    self.ee_gripper_pub.publish(gripper_msg)
                else:
                    gripper_msg.data = False
                    self.ee_gripper_pub.publish(gripper_msg)

                if self.counter < len(self.waypoints[self.actual_waypoint]['time']) - 1:
                    self.ti = self.tf
                    self.t = self.ti
                    self.counter += 1
                    self.tf = self.waypoints[self.actual_waypoint]['time'][self.counter]
                else:
                    # Pass to another trajectory
                    self.get_logger().info("Finish trajectory for the " + str(self.actual_waypoint))
                    if (self.count_waypoints < len(self.waypoints) - 1):
                        self.count_waypoints += 1
                        self.actual_waypoint = list(self.waypoints.keys())[self.count_waypoints]
                        # Reset Iterable Variables
                        self.get_logger().info("Planning trajectory for the " + str(self.actual_waypoint) + " cube.")
                        self.counter = 1
                        self.ti = self.waypoints[self.actual_waypoint]['time'][self.counter - 1]
                        self.t = self.ti
                        self.tf = self.waypoints[self.actual_waypoint]['time'][self.counter]
                    else:
                        self.get_logger().info("All trajectories completed")
                        self.timer.destroy()
                        sys.exit(0)
            
    def compute_ee_orientation(self, quat_i, quat_f):
            '''
            Compute EE orientation in time
            quat_i = initial quaternion of the first point
            quat_f = final quaternion of the second point
            '''
            quat_i = R.from_quat(quat_i)
            Ri = R.as_matrix(quat_i) # initial rotation matrix

            quat_f = R.from_quat(quat_f)
            Rf = R.as_matrix(quat_f) # final rotation matrix

            Rif = np.matmul(np.transpose(Ri), Rf)

            r11 = Rif[0,0]
            r12 = Rif[0,1]
            r13 = Rif[0,2]

            r21 = Rif[1,0]
            r22 = Rif[1,1]
            r23 = Rif[1,2]

            r31 = Rif[2,0]
            r32 = Rif[2,1]
            r33 = Rif[2,2]

            theta = np.arccos(
                (r11 + r22 + r33 - 1)/2
            )

            r = (1/(2*np.sin(theta)))*np.matrix([[r32 - r23], [r13 - r31], [r21 - r12]])
            theta_t = self.cubic_polynomial(0, theta, 0, 0, self.ti, self.tf, self.t)

            rx = r[0, 0]
            ry = r[1, 0]
            rz = r[2, 0]
            
            R11 = rx**2*(1 - np.cos(theta_t)) + np.cos(theta_t)
            R12 = rx*ry*(1 - np.cos(theta_t)) - rz*np.sin(theta_t)
            R13 = rx*rz*(1 - np.cos(theta_t)) + ry * np.sin(theta_t)

            R21 = rx*ry*(1 - np.cos(theta_t)) + rz * np.sin(theta_t)
            R22 = ry**2*(1 - np.cos(theta_t)) + np.cos(theta_t)
            R23 = ry*rz*(1 - np.cos(theta_t)) - rx * np.sin(theta_t)

            R31 = rx*rz*(1 - np.cos(theta_t)) - ry * np.sin(theta_t)
            R32 = ry*rz*(1-np.cos(theta_t)) + rx*np.sin(theta_t)
            R33 = rz**2*(1 - np.cos(theta_t)) + np.cos(theta_t)

            Rit = np.matrix([[R11, R12, R13], [R21, R22, R23], [R31, R32, R33]])
            Re = np.matmul(Ri, Rit)

            final = R.from_matrix(Re)
            phi_ee = final.as_quat()
            return phi_ee

            
    def compute_ee_position(self, pi, pf):
        '''
        Compute Trajectory as EE position given the initial position and final position of the EE
        '''
        pi = np.array(pi)
        pf = np.array(pf)

        L = np.linalg.norm(pf - pi)
        s = self.cubic_polynomial(0, L, 0, 0, self.ti, self.tf, self.t)
        
        # pe = pi + s * ((pf - pi)/norm(pf - pi)) ---> Linear Trajectory
        pe =  pi + s * ((pf - pi)/L)
        return pe


    def cubic_polynomial(self, qi, qf, qid, qfd, ti, tf, t):
        a0 = qi
        
        a1 = qid

        a2 = (3*(qf - qi) - (2*qid + qfd)*(tf - ti))/((tf-ti)**2)

        a3 = (-2*(qf - qi) + (qid + qfd)*(tf-ti))/((tf-ti)**3)

        q = a3*(t - ti)**3 + a2*(t - ti)**2 + a1*(t-ti) + a0
        return q

    def handle_keyboard(self):
        tempDict={}
        tempDict['setup'] = self.waypoints['setup']
        check1 = False
        check2 = False
        check3 = False
        check4 = False
        while True:
            if not check1 or not check2 or not check3 or not check4:
                print('\n- Choose robot pickup order -')
            if not check1: print('   1. Command (Green cube)')
            if not check2: print('   2. Command (Red cube)')
            if not check3: print('   3. Command (Yellow cube)')
            if not check4: print('   4. Command (Blue cube)')

            menu = input('Input the menu: ')

            if(menu == '1'):
                tempDict['green'] = self.waypoints['green']
                check1 = True
            elif(menu == '2'):
                tempDict['red'] = self.waypoints['red']
                check2 = True
            elif(menu == '3'):
                tempDict['yellow'] = self.waypoints['yellow']
                check3 = True
            elif(menu == '4'):
                tempDict['blu'] = self.waypoints['blu']
                check4 = True
            else:
                pass

            if check1 and check2 and check3 and check4:
                self.waypoints.clear() 
                self.waypoints.update(tempDict)

                partDitc = {'positions' : [0],'orientations' : [0],}

                # know the order of the read dict input, we need it later
                for key in tempDict:
                    self.orderDict[key] = partDitc.copy()
                tempDict.clear()

                self.count_waypoints = 0
                self.counter = 1
                self.actual_waypoint = list(self.waypoints.keys())[self.count_waypoints]

                self.ti = self.waypoints[self.actual_waypoint]['time'][self.counter - 1]
                self.t = self.ti
                self.tf = self.waypoints[self.actual_waypoint]['time'][self.counter]

                self.unlockedPath2 = True
                break

    def startThread(self):
        th = threading.Thread(target=self.handle_keyboard)
        th.start()

def main(args=None):
    rclpy.init(args=args)

    trajectory_pub = Trajectory()

    rclpy.spin(trajectory_pub)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    trajectory_pub.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()