function [points] = generate_random_points(n_points, c, r)
% generate_random_points returns a number of points for the 
% given sphere of CENTER c and RADIUS r
    min_theta = 0;
    max_theta = pi;
    min_phi = 0;
    max_phi = 2*pi;
    points = [];
    for i = 1:n_points
        theta = (max_theta - min_theta).*rand() + min_theta;
        phi =(max_phi - min_phi).*rand() + min_phi;
        x = c(1) + r*sin(theta)*cos(phi);
        y = c(2) + r*sin(theta)*sin(phi);
        z = c(3) + r*cos(theta);
        points = [points, [x;y;z]];
    end
end

