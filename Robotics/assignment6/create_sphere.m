function [X, Y, Z] = create_sphere(center, radius)
% create_sphere returns the x-, y-, and z- coordinates of a sphere translated in center with the given radius without drawing it.
    [X, Y, Z] = sphere(50);
    X = X * radius + center(1);
    Y = Y * radius + center(2);
    Z = Z * radius + center(3);
end

