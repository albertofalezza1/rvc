function [p] = compute_arc(points,p0,r)

for i= 1:size(points, 2)-1
    pi = points(:, i);
    pf = points(:, i+1);
    
    xi = (pi - p0) / norm(pi - p0); % x prime axis is along the first point
    xf = (pf - p0) / norm(pf - p0); % use the second point to compute the z prime axis
    zp = cross(xi, xf); % z prime axis
    zp = zp/norm(zp); 
    yi = cross(zp, xi); % now compute the correct y prime axis normal to z prime and x prime
    yi = yi/norm(yi);

    R = [xi yi zp]; % Rotation matrix of Ep wrt Eb. Ep is frame prime and Eb base frame
    
    Ri = [zp, cross(-zp, -xi), -xi]; % Rotation Ei wrt Eb -> EE frame -> [ni si ai]
    Rf = [zp, cross(-zp, -xf), -xf]; % Rotation Ef wrt Eb -> EE frame -> [nf sf af]
    Rif = transpose(Ri)*Rf; % Rotation from initial to final frame wrt Ei
    theta = acos( ( Rif(1,1) + Rif(2,2) + Rif(3,3) - 1 ) / 2);
    w = ( 1/(2*sin(theta)) ) * [ Rif(3,2) - Rif(2,3); 
                                 Rif(1,3) - Rif(3,1); 
                                 Rif(2,1) - Rif(1,2)]; % Axis of rotation wrt 

    s = 0:0.05:theta;
    
    % Compute the path along the two points as an arc length circular path
    x = r * cos(s);
    y = r * sin(s);
    z = zeros([1, size(s, 2)]);
    
    pp = [x;y;z];

    % Compute the rotation matrix wrt the base frame Eb
    p = p0 + R*pp; % Compute the new coordinate system
    
    % If you want n, t, b is to verify that EE frame orientation coherent
    % with Frenet computed by Hand
    
%     n = (p - p0)/norm(p - p0);
%     n = -n;
%     t = R*[-sin(s/r); cos(s/r);zeros(size(s))];
%     t = t/norm(t);
%     b = cross(t, n);
%     b = b / norm(b)

    % Compute the Frenet Frame for each sampled angle
    % Ei is the frame attacched to the first point on the path
    % Ef is the frame attached to the second point on the path
    
    f11 = w(1).^2 * (1 - cos(s)) + cos(s);
    f11 = reshape(f11, [1, 1, size(s, 2)]);
    
    f12 = w(1) * w(2) * ( 1 - cos(s)) - w(3) * sin(s);
    f12 = reshape(f12, [1, 1, size(s, 2)]);
    
    f13 = w(1) * w(3) * (1 - cos(s)) + w(2) * sin(s);
    f13 = reshape(f13, [1, 1, size(s, 2)]);
    
    f21 = w(1) * w(2) * (1-cos(s)) + w(3) * sin(s);
    f21 = reshape(f21, [1, 1, size(s, 2)]);
    
    f22 = w(2).^2 * (1 - cos(s)) + cos(s);
    f22 = reshape(f22, [1, 1, size(s, 2)]);
    
    f23 = w(2) * w(3) * (1 - cos(s)) - w(1) * sin(s);
    f23 = reshape(f23, [1, 1, size(s, 2)]);
    
    f31 = w(1) * w(3) * (1 - cos(s)) - w(2) * sin(s);
    f31 = reshape(f31, [1, 1, size(s, 2)]);
    
    f32 = w(2) * w(3) * (1 - cos(s)) + w(1) * sin(s);
    f32 = reshape(f32, [1, 1, size(s, 2)]);
    
    f33 = w(3).^2 * (1 - cos(s)) + cos(s);
    f33 = reshape(f33, [1, 1, size(s, 2)]);
    
    Ft = [
        f11 f12 f13; 
        f21 f22 f23; 
        f31 f32 f33
        ];
       
    for i=1:size(Ft,3)
        
        Rt = Ri * Ft(:,:,i);
        Rt(:, 1) = Rt(:, 1)/norm(Rt(:, 1));
        Rt(:, 2) = Rt(:, 2)/norm(Rt(:, 2));
        Rt(:, 3) = Rt(:, 3)/norm(Rt(:, 3));
        quiver3(p(1, i), p(2,i), p(3, i), Rt(1,1),Rt(2,1),Rt(3,1),'Color','red')
        quiver3(p(1, i), p(2,i), p(3, i), Rt(1,2),Rt(2,2),Rt(3,2),'Color','green')
        quiver3(p(1, i), p(2,i), p(3, i), Rt(1,3),Rt(2,3),Rt(3,3), 'Color','blue')
    end

%     TO PLOT n, t, b to test
%     quiver3(p(1, :), p(2,:), p(3, :), n(1, :), n(2, :), n(3, :), 0, 'Color','red')
%     quiver3(p(1, :), p(2,:), p(3, :), t(1, :), t(2, :), t(3, :), 0, 'Color','red')
%     quiver3(p(1, :), p(2,:), p(3, :), b(1, :), b(2, :), b(3, :), 0, 'Color','green')
end

