%% ASSIGNMENT 6
% Let p1, p2, p3 three points on a sphere of center p0 and radius R. Design the
% trajectory such that (1) the EE will pass through the three points along the shortest
% path, and (2) the z axis of the EE is always orthogonal to the sphere.
close all;
clear;
clc;
%% Define the sphere
% Center p0 as a 3 by 1 vector of coordinates in the interval [0, 10]
p0 = rand(3,1) * 10
% Define the radius as a scalar in the interval [1, 5]
min = 1; % Minimum radius of 1
max = 5; % Maximum radius of 5
r = (max - min).*rand() + min % Radius definition
% Define the sphere in center p0, with radius r using the defined function
[X, Y, Z] = create_sphere(p0, r);

% Plot the centered and translated sphere
sphere_surf = surf(X, Y, Z);
sphere_paint = [0 0 0];
colormap(sphere_paint); % to have a uniform color for the sphere plot
set(sphere_surf, 'FaceAlpha', 0.3, 'EdgeColor', 'none');
axis equal
hold on

%% Define 3 points on the sphere p1, p2, p3
% Generate 3 random points to then define a trajectory to follow
n_points = 3;
points = generate_random_points(n_points, p0, r)

% plot the points on the sphere
scatter3(p0(1), p0(2), p0(3), 100, 'w', '*');

for i=1:size(points, 2)
    scatter3(points(1, i), points(2, i), points(3, i), 80, 'filled');
end

hold on

%% Compute arc of circunference between the 3 points to go from p1 to p2 and from p2 to p3
% The computations follow the slides of L7 and L8 lectures for arc tracing
% and EE orientation

% we pass the points, the center and the radius
p = compute_arc(points, p0, r);

for i= 1:size(points, 2)

    path_plot = plot3(p(1, :), p(2,:), p(3,:));
    path_plot.Color = 'white';
    path_plot.LineWidth = 3;
end




