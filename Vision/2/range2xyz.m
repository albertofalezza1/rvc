close all
clear
clc
DIZ = imread('0000001-000000000000.png'); % depth image Z coordinate
rgbImage = imread('0000001-000000000000.jpg'); %rgb image

figure(1)
imagesc(DIZ);
% kinect people pgm file
fx = 525; %focal lenght in pixels
cx = 319.5; % coordinate of the principal point
fy = 525;
cy = 239.5;
cameraParams = [fx fy cx cy];

nrow = size(DIZ,1);
ncol = size(DIZ,2);
cloud = zeros(nrow*ncol,3);
X = zeros(nrow, ncol);
Y = zeros(nrow, ncol);
TRUE = zeros(nrow,ncol);
k = 0;
%DephTH = 4000;
DephTH = 2000;

% the Z is already given, so we need to map X and Y
for i = 1:1:ncol %column
    for j = 1:1:nrow %row
        % projection principles
        X(j,i) = -(i - cx) * double(DIZ(j,i))/fx;
        Y(j,i) = -(j - cy) * double(DIZ(j,i))/fy;
        if (DIZ(j,i)~=0 && DIZ(j,i)<DephTH) % threshold to keep
            TRUE(j,i)=1;
            k=k+1;
            cloud(k,:) = [X(j,i) Y(j,i) double(DIZ(j,i))];
        end
    end
end

cloud2 = cloud(1:k,:);
figure(2);
plot3(cloud2(:,1), cloud2(:,2), cloud2(:,3), 'r.');
axis equal

% store the cloud of point
npoint = size(cloud2,1);
Triangle=[];
exportMeshToPly(cloud2, Triangle, ones(npoint,3), 'PC_out');

% estimate the mesh
[vertices, faces, color] = rangetomesh(rgbImage, DIZ, cameraParams, DephTH);

% plot mesh
figure(3)
plottedMesh = trisurf(faces,vertices(:,1),vertices(:,2),vertices(:,3));
plottedMesh.FaceVertexCData = color;
set(plottedMesh,'LineStyle','none')
hold on
axis equal

% store the mesh
exportMeshToPly(vertices, faces, color, 'Mesh_out');
