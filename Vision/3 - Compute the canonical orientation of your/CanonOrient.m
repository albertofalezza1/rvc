clc;
close all;
clearvars;
% Load point cloud and downsample to 10% of the original points
ptCloud = pcread("PC_zep.ply");
ptCloud = pcdownsample(ptCloud, "random", 0.1);
%figure(1);
%pcshow(ptCloud)
%hold on;
P = ptCloud.Location;

% Object-oriented tools for fitting conics and quadrics add-ons
% Since our point cloud is is not perfectly coplanar, we need to fit a 
% plane to it in order to determine the necessary rotation.
% pfit=planarFit(P');
% x0=mean(P);
% R=pfit.vecrot(pfit.normal,[0,0,-1]);
% result=pointCloud((P-x0)*R.'+x0);
% pcshow(result)
%% EXTRINSIC CENTROID
X = ptCloud.Location(:,1);
Y = ptCloud.Location(:,2);
Z = ptCloud.Location(:,3);
P0 = [mean(X) mean(Y) mean(Z)];
figure(1);
pcshow(P); axis equal; hold on;
scatter3(P0(1), P0(2), P0(3),150, 'm', 'filled'); 
hold on;

%% CANONICAL BASIS
%  it can be shown that the principal components are eigenvectors of the 
% data's covariance matrix. Thus, the principal components are often 
% computed by eigendecomposition of the data covariance matrix or singular 
% value decomposition of the data matrix

% Computation of the covariance matrix
dX = bsxfun(@minus, P, P0);
C = dX' * dX;
% PCA - The columns of U are, in order, d1, d2 and d3
% the columns of U are orthogonal 
[U,~]=svd(C);

%% CANONICAL ORIENTATION
% Rotation matrix
R = -U';
% Translation vector
t = U'*P0';
Pc = R*P' - t;
Pc0 = R*P0' - t;
pcshow(Pc'); hold on;
scatter3(Pc0(1), Pc0(2), Pc0(3),150, 'm', 'filled'); hold on;

Triangle=[];
npoint = size(Pc.',1);
exportMeshToPly(Pc.', Triangle, ones(npoint,3), 'PC_out');

title('Zephyr orientation (blue) vs canonical orientation (yellow), centroids in magenta');