function visual_plane(plane_normal, plane_distance, val, centre3d)
   w = null(plane_normal);              % Find two orthonormal vectors which are orthogonal to v
   [P,Q] = meshgrid(-val:val);          % Provide a gridwork (you choose the size)
   X = centre3d(1)+w(1,1)*P+w(1,2)*Q;   % Compute the corresponding cartesian coordinates
   Y = centre3d(2)+w(2,1)*P+w(2,2)*Q;   % using the two vectors in w
   Z = centre3d(3)+w(3,1)*P+w(3,2)*Q;
   
   skip = 20 ;                          % skip these many number rows and columns, change it accordingly
   XX = X(1:skip:end,1:skip:end) ;
   YY = Y(1:skip:end,1:skip:end) ;
   ZZ = Z(1:skip:end,1:skip:end) ;
   
   h = surf(XX,YY,ZZ, 'FaceAlpha',1, 'EdgeColor', [0.4940 0.1840 0.5560], 'FaceColor', 'none');
   %set(h,'LineStyle','none')
   grid on;
end

