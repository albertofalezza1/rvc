close all
clear
clc

I = imread('0000001-000000000000.jpg');
figure(1)
imshow(I);
%
DIZ = imread('0000001-000000000000.png'); % Depth Image z coordinate
figure(2);
imagesc(DIZ);

%focal length is 525 for both axes and the principal point is (319.5, 239.5)
% camera parameters
fx=525;
cx=319.5;
fy=525;
cy=239.5;

% we analyze the cloud of points
nrow = size(DIZ, 1);
ncol = size(DIZ, 2);
DIZseg=zeros(nrow,ncol);
cloud = zeros(nrow*ncol, 3);
X = zeros(nrow, ncol);
Y = zeros(nrow ,ncol);
TRUE = zeros(nrow, ncol);
k=0;
DephTH=643; % reasonable choice for this image (goes from 0 to ~4500)

for i = 1:1:ncol %column
    for j = 1:1:nrow %row
        % see projection principles
        X(j,i) = -(i -cx) * double(DIZ(j,i))/fx;
        Y(j,i) = -(j -cy) * double(DIZ(j,i))/fy;
        if (DIZ(j,i)~=0 && DIZ(j,i)<DephTH) % select only the pixels that are less then the threshold
            DIZseg(j,i)=DIZ(j,i);
            TRUE(j,i)=1;
            k=k+1;
            cloud(k, :)=[X(j,i) Y(j,i) double(DIZ(j,i))];
        end
    end
    % disp(i)
end

% Better use TRUE struct for DIZseg
figure(3); % so here we can see the regions
imagesc(DIZseg);
%
cloud2=cloud(1:k,:);
figure(4)
plot3(cloud2(:,1), cloud2(:,2), cloud2(:,3), 'r'); % and we can have the cloud of points (associated to the selected region)
axis equal

bw =imbinarize(DIZseg); %we binarize the image, we treat the range image as a standard 2D image
figure(5);
imshow(bw);
bw = bwareaopen(bw,50); %we remove small unconneted components (more clear reion)
figure(5);
imshow(bw);

% we now can start analyze the region props
out = regionprops(bw, 'Centroid', 'Extrema', 'Orientation', 'BoundingBox');

bb = [out.BoundingBox(1), out.BoundingBox(2);
      out.BoundingBox(1)+ out.BoundingBox(3), out.BoundingBox(2);
      out.BoundingBox(1)+ out.BoundingBox(3), out.BoundingBox(2) + out.BoundingBox(4);
      out.BoundingBox(1), out.BoundingBox(2)+ out.BoundingBox(4);
      out.BoundingBox(1), out.BoundingBox(2);
     ];
 hold on %here we have the boundingBox
 plot(bb(:,1),bb(:,2),'c');
 plot(bb(:,1),bb(:,2),'co');
 %
 % boundary
 bound = imcontour(bw); %here we have the contour of the figure
 plot(bound(1,:), bound(2,:),'c.');
 centre2d=out.Centroid; % here we have the centre of the figure
 hold on
 plot(centre2d(1), centre2d(2), 'c.', 'MarkerSize',30);
 hold
 % orientation
 angle = degtorad(out.Orientation); % we compute the angles 
 main_orient=[cos(angle), -sin(angle)];
 hold on
 quiver(centre2d(1), centre2d(2), main_orient(1), main_orient(2), 30);
 main_orient_p=[centre2d(1)+50*main_orient(1), centre2d(2)+50*main_orient(2)]; %we compute the main orientation
 hold on
 plot(main_orient_p(1), main_orient_p(2), 'b.', 'MarkerSize', 30);
 % extrema
 extr = out.Extrema; % we compute the extreme points 
 hold on
 plot(extr(:,1), extr(:,2), 'r.', 'MarkerSize', 30);
 % fitting
 % 3d domain
 disp('Search plane.');
 %[inliers_p, outliers_p] = ransac_plane(cloud2, 20.0, 450, 50, 1);
 [plane_normal, plane_distance, mu] = fit_plane(cloud2); % we plot the plane of the 3D points
 % Rendering
 figure(6);
 set(gcf, 'Renderer', 'OpenGL');
    hold on
    grid on
    
    plot3(cloud2(:,1), cloud2(:,2), cloud2(:,3), 'r.'); % we plot the point cloud
    
    % centroid 3D
    centre3d = mean(cloud2);
    visual_plane(plane_normal, plane_distance, 100.0, centre3d); %we visualize the plane accordingly to the cloud point
    % normal
    normal_p = centre3d-20.*plane_normal; % we found the normal and plot it later
    axis equal
    hold on
    plot3([centre3d(1); normal_p(1)], [centre3d(2); normal_p(2)], [centre3d(3); normal_p(3)]);
    plot3(centre3d(1), centre3d(2), centre3d(3), 'b.', 'MarkerSize', 30); % plot of the centre
    plot3(normal_p(1), normal_p(2), normal_p(3), 'b.', 'MarkerSize', 20); % plot of the normal
    % so now we have the position and the orientation of the plane
    % plane orientation
    index = round(main_orient_p);
    orient_p3d=[X(index(2), index(1)),Y(index(2),index(1)), double(DIZ(index(2),index(1)))];
    %
    plot3(orient_p3d(1), orient_p3d(2), orient_p3d(3), 'c.', 'MarkerSize', 25); % we plot another point for the main orientation
    %
    check_planar = dot(plane_normal, (orient_p3d-centre3d));
    disp(check_planar); % so we can find the line from the center to the point
    
    % put point to plane:
    cloud_proj=zeros(k,3);
    for i=1:k
        %P_p=P-<(P-C),n>*n
        cloud_proj(i,:)=cloud2(i,:)-(dot((cloud2(i,:)-centre3d),plane_normal)*plane_normal);
    end
    hold on
    plot3(cloud_proj(:,1), cloud_proj(:,2), cloud_proj(:,3), 'y.'); %we can also plot the 3D points to the plane
    
    % take boundaries
    bound3d = zeros(length(bound),3);
    bound3d_pj = zeros(length(bound), 3);
    index = round(bound)';
    b=0;
    for i=1:length(bound)
        if((index(i,1)>3)&&TRUE(index(i,2),index(i,1))==1)
            temp=[X(index(i,2), index(i,1)), Y(index(i,2), index(i,1)), double(DIZ(index(i,2), index(i,1)))];
            if(temp~=[0,0,0])
                b=b+1;
                bound3d(b,:)=temp;
                % project boundary point to plane
                bound3d_pj(b,:)=temp-(dot((temp-centre3d),plane_normal)*plane_normal);
            end
        end
    end
    bound3d=bound3d(1:b,:);
    bound3d_pj=bound3d_pj(1:b,:);
    % all this calculation are made to project thhe boundaries of our point
    % cloud
    plot3(bound3d(:,1), bound3d(:,2), bound3d(:,3), 'c.');
    plot3(bound3d_pj(:,1), bound3d_pj(:,2), bound3d_pj(:,3), 'm.');
    
    % RANSAC LINE FITTING

    t=-250:1:250;

    % ransac_line(data, minNumInliers, numIteration, tresh)
    % from the boundaries we estimate a line
    [startPoint1, direction1, inliers, outliers]=ransac_line(bound3d, 50, 150, 5);
    ransacLine1=startPoint1+t.*direction1;
    % we compute ransac using the outliers of the previous step, since we
    % used the full bound in the previous step
    [startPoint2, direction2, inliers, outliers]=ransac_line(outliers, 50, 150, 5);
    ransacLine2=startPoint2+t.*direction2;

    [startPoint3, direction3, inliers, outliers]=ransac_line(outliers, 50, 150, 5);
    ransacLine3=startPoint3+t.*direction3;

    [startPoint4, direction4, inliers, outliers]=ransac_line(outliers, 50, 150, 5);
    ransacLine4=startPoint4+t.*direction4;

    % here we plot te line we got from ransac alghoritm
    plot3(ransacLine1(1,:),ransacLine1(2,:),ransacLine1(3,:),'.','Color','g');
    plot3(ransacLine2(1,:),ransacLine2(2,:),ransacLine2(3,:),'.','Color','c');
    plot3(ransacLine3(1,:),ransacLine3(2,:),ransacLine3(3,:),'.','Color','m');
    plot3(ransacLine4(1,:),ransacLine4(2,:),ransacLine4(3,:),'.','Color','y');
    
    %RANSAC LINE INTERSECTION
    % compute point of line 1 which is closer to line 2 not parallel, then
    % projected this point to line 2, done because these line may be not lying
    % on the same plane

    matrixOfLines=[ransacLine1;ransacLine2;ransacLine3;ransacLine4];
    matrixOfDirection=[direction1,direction2,direction3,direction4];

    for i=1:4
        for j=1:4
            if(j>i) %we find which lines are orthogonal
                angle = acosd(dot(matrixOfDirection(:,j),matrixOfDirection(:,i))/(norm(matrixOfDirection(:,j))*norm(matrixOfDirection(:,i))));
                if(angle>70 && angle<160)
                    distVec=[];
                    line1=matrixOfLines(i*3-2:i*3,:);
                    line2=matrixOfLines(j*3-2:j*3,:);
                    for index=1:size(matrixOfLines,2) %we search the distance vector from the lines we are considering
                        distVec(index)=norm(cross(line2(:,1)-line2(:,end),line1(:,index)-line2(:,1)))/norm(line2(:,1)-line2(:,end));
                    end
                    [~,I] = min(distVec); %we select the min so we have the right position
                    
                    % we compute the line intersection
                    A = line2(:,1);
                    AP= line1(:,I(1))-line2(:,1);
                    AB= line2(:,end)-line2(:,1);
                    
                    % we proect every point
                    projection= A + dot(AP,AB) / dot(AB,AB) * AB;
                    plot3(projection(1),projection(2),projection(3),'.','Color','k','MarkerSize',35);
                    % one we have all the intersections we have all the
                    % information we need to define, detect and orient the
                    % object in the camera position, since everything is
                    % expressed in camera position
                end
            end
        end
    end
