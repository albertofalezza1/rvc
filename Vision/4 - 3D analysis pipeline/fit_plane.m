function [plane_normal,plane_distance, mu] = fit_plane(data)
    
    mu = mean(data,1);
    % the columns of V are orthogonal
    [~,~,V]=svd(data-mu,0);
    plane_normal=V(:,end).';
    plane_distance=plane_normal*mu';
    
end
