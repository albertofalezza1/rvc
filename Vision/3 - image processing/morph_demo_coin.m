close all;
clear;
clc;

% Read the image with coins into the workspace
I = imread('eight.tif');
figure(1);
imshow(I);

% As first step, we remove all of the foreground (coins) using
% morphological opening.
% The opening operation removes small obects that cannot completely contain
% the structuring element. Define a disk-shped structuring element with
% radius of 10, which fits entirely inside a single coin

% For coins
se = strel('disk', 10);

% to perform the morphological opening, use imopen with the structuring
% element
% we get an homogeneous region for the coin and the background
I2 = imopen(I,se);
figure(2);
imshow(I2);

% Use imadjust to increase the contrast of the processed image I2 by
% saturing 1% of the data at both low and high intesities and by stretching
% the intensity values to fill the uint8 dynamic range

I3 = imadjust(I2);
figure(3);
imshow(I3);

% Create a binary version of the processed image, we use imbinarize
% function to convert the grayscale image into a binary image. Remove
% background noise from the image with the bwareaopen function.

% we do this because 1 is white and 0 is dark, so we have to invert the
% color to make matlab understand which is the background and the
% foreground. Otherwise later we will find only one big connected
% component
bw = not(imbinarize(I3));
bw = bwareaopen(bw,50);
figure(5);
imshow(bw);

% Find all the connected components (objects) in the binary image.

cc = bwconncomp(bw,4);
cc.NumObjects

% view the coin that is labeled ind1 in the image

 indl = 2;
 coin = false(size(bw));
 coin(cc.PixelIdxList{indl}) = true;
 figure(6);
 imshow(coin);
 % Visualize all the connected components in the image by creating a label
 % matrix and then displaying it as a pseudocolor inexed image
 labeled = labelmatrix(cc);
 whos labeled;
 RGB_label = label2rgb(labeled,'spring','c','shuffle'); 
 % we can label and associate the color, now we have new informations
 % we can clearly see the obects and separate from each others
 figure(7);
 imshow(RGB_label);
 
% Compute the area of each object in the image using regionprops.
% Each coins is one connected component in the cc structure.
% we extract the region properties
 coindata = regionprops(cc,'basic');
 % Areas
 coin_areas = [coindata.Area];
 % find and display min area
[min_area,idx] = min(coin_areas);
coin = false(size(bw));
coin(cc.PixelIdxList{idx}) = true;
figure(8)
imshow(coin);

% Use the histogram command to create a histogram of coin areas
histogram(coin_areas);
title('Histogram of Coin Area');

% make statistic on coins:
% we extract other region propreties
stats = regionprops(cc,'Centroid','MajorAxisLength','MinorAxisLength','Circularity');
figure(7)
% for each region we compute the center, the diamater, the radius and thhe
% circle around them
for i=1:length(stats)
    centers(i,:)=stats(i).Centroid;
    diameters(i) = mean([stats(i).MajorAxisLength stats(i).MinorAxisLength],2);
    radii(i) = diameters(i)/2;
    circ(i)=stats(i).Circularity;
    hold on
end
% we now know the geometric propreties of the object we are studying,
% we know the location and the shape, thanks to radius
% Ex. if the robot has to pick this object, now has full information of it
viscircles(centers, radii);
hold off