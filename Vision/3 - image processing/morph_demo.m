close all;
clear;
clc;

% Read an image into the workspace
I = rgb2gray(imread('rice.png'));
figure(1);
imshow(I);

% Remove all of the foreground using morphological opening.
% The opening operation removes small objects that cannot completely
% contain the structuring element.
% Define a disk-shaped structuring element with a radius of 15, which fits
% entirely inside a single grain of rice.

% se = strel('disk', 15);
% se = strel('disk', 3);
% se = strel('disk', 7);
% se = strel('disk', 5);
 se = strel('disk', 20);

% To perform the morphological opening, use imopen with thhe structuring
% element

background = imopen(I,se);
figure(2);
imshow(background);

% Subtract the background approximation image, background from thhe
% original image, I.
% After subtracting the adjusted background image from thhe original
% image, the resulting image has a uniform background

I2 = I - background;
figure(3);
imshow(I2);

% Use imadjust to increase the contrast of the processed image I2 by
% saturing 1% of the data at both low and high intesities and by stretching
% the intensity values to fill the uint8 dynamic range

I3 = imadjust(I2);
figure(4);
imshow(I3);

% The prior two steps could be replaced by a single step using imtophat

% I2 = imtopath(I,strel('disk',15));

% Create a binary version of the processed image, we use imbinarize
% function to convert the grayscale image into a binary image. Remove
% background noise from the image with the bwareaopen function.

bw = imbinarize(I3);
bw = bwareaopen(bw,50);
figure(5);
imshow(bw);

% Find all the connected components in the binary image.

cc = bwconncomp(bw,4);
cc.NumObjects

% view the rice grain that is labeled 50 in the image

 indl = 50;
 
 grain = false(size(bw));
 grain(cc.PixelIdxList{indl}) = true;
 figure(6);
 imshow(grain);
 % Visualize all the connected components in the image by creating a label
 % matrix and then displaying it as a pseudocolor inexed image
 labeled = labelmatrix(cc);
 whos labeled;
 RGB_label = label2rgb(labeled,'spring','c','shuffle');
 figure(7);
 imshow(RGB_label);
 
 % Compute the area of each object in the image using regionprops.
 % Each rice grain is one connected component in the cc structure.
 graindata = regionprops(cc,'basic');
 % Areas
 grain_areas = [graindata.Area];
 % find and display min area
[min_area,idx] = min(grain_areas);
grain = false(size(bw));
grain(cc.PixelIdxList{idx}) = true;
figure(8);
imshow(grain);

% Use the histogram command to create a histogram of rice grain areas
figure(9);
histogram(grain_areas);
title('Histogram of Rice Grain Area');