close all;
clear;
clc;

% Read an image into the workspace
I = rgb2gray(imread('myImgBW.png'));
figure(1);
imshow(I);

% As first step, we remove all of the foreground (coins) using
% morphological opening.
% The opening operation removes small obects that cannot completely contain
% the structuring element. Define a disk-shped structuring element with
% radius of 70, which fits entirely inside a single coin

% For coins
se = strel('disk', 10);

I2 = imopen(I,se);
figure(2);
imshow(I2);

% Use imadjust to increase the contrast of the processed image I2 by
% saturing 1% of the data at both low and high intesities and by stretching
% the intensity values to fill the uint8 dynamic range

I3 = imadjust(I2);
figure(3);
imshow(I3);

% Create a binary version of the processed image, we use imbinarize
% function to convert the grayscale image into a binary image. Remove
% background noise from the image with the bwareaopen function.

bw = not(imbinarize(I3));
bw = bwareaopen(bw,50);
bw = imdilate(bw,se);
figure(5);
imshow(bw);

% Find all the connected components in the binary image.

cc = bwconncomp(bw,4);
cc.NumObjects % n connected components

% view the coin that is labeled ind1 in the image
% we take 1 coin and visualize as before
 indl = 2;
 coin = false(size(bw));
 coin(cc.PixelIdxList{indl}) = true;
 figure(6);
 imshow(coin);
 % Visualize all the connected components in the image by creating a label
 % matrix and then displaying it as a pseudocolor inexed image
 labeled = labelmatrix(cc);
 whos labeled;
 RGB_label = label2rgb(labeled,'spring','c','shuffle');
 figure(7);
 imshow(RGB_label);
 
% Compute the area of each object in the image using regionprops.
 % Each object is one connected component in the cc structure.
 coindata = regionprops(cc,'basic');
 % Areas
 coin_areas = [coindata.Area];
 % find and display min area object
[min_area,idx] = min(coin_areas);
coin = false(size(bw));
coin(cc.PixelIdxList{idx}) = true;
figure(8)
imshow(coin);

% make statistic on objects: to understand how the object is oriented and
% separate the coins from the other objects
stats = regionprops(cc,'Centroid','MajorAxisLength','MinorAxisLength','Circularity');
%
% Analyse shape:
% find coins and driver
MIN=1000;
minind=0;
for i=1:length(stats)
    % we analyze the prop. circularity and we select the region with the
    % least circularity (the driver)
    circ(i)=stats(i).Circularity;
    if(circ(i)<MIN)
        MIN=circ(i);
        minind=i;
    end
end
disp('John Cena shape: ');
disp(minind);
% we found john cena is the region number 3, so we define the center of
% this region
centers(minind,:) = stats(minind).Centroid;
figure(1)
hold on;
plot(round(centers(minind,1)),round(centers(minind,2)), 'r.', 'MarkerSize', 30);
figure(1)
hold on;
plot(round(centers(minind,1)),round(centers(minind,2)), 'r.', 'MarkerSize', 30);
hold on;
text(round(centers(minind,1)),round(centers(minind,2)),'\leftarrow John Cena','FontSize',16,'FontWeight','bold');
indcir=ones(length(stats),1);
indcir(minind)=0;

figure(1)
for i=1:length(stats)
    if(indcir(i)==1) %since the circularity of the usb drive is 0 it will not draw a circle
        centers(i,:)=stats(i).Centroid; %we compute the centroids
        diameters(i) = mean([stats(i).MajorAxisLength stats(i).MinorAxisLength],2); %we compute the diameters
        radii(i) = diameters(i)/2; %we compute the radius
    end
end
% plot label:
maxRadiusIndex = find(radii == (max(radii))); % get the maximus radius index
hold on
viscircles(centers(maxRadiusIndex,:),radii(maxRadiusIndex)); %we draw the circle around each coins

figure(1)
hold on;
plot(round(centers(maxRadiusIndex,1)),round(centers(maxRadiusIndex,2)), 'r.', 'MarkerSize', 30);
hold on;
text(round(centers(maxRadiusIndex,1)),round(centers(maxRadiusIndex,2)),'\leftarrow Bigger coin','FontSize',16,'FontWeight','bold'); %label on the big coin
hold on;
%
di=find(diameters>0);
[drop,ind]=min(diameters(di));
ind2 = di(ind);

% for better view
fixedQuad = radii(ind2)*1.3;
rectangle('Position',[round(centers(ind2,1)-fixedQuad/2) round(centers(ind2,2)-fixedQuad/2) fixedQuad fixedQuad], 'LineWidth',2, 'EdgeColor','r', 'Curvature',0.2)
hold on;
plot(round(centers(ind2,1)),round(centers(ind2,2)), 'r.', 'MarkerSize', 30);
hold on;
text(round(centers(ind2,1)),round(centers(ind2,2)),'\leftarrow Square','FontSize',16,'FontWeight','bold'); %label of thhe square

